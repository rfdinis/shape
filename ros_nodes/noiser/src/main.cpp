#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Float32MultiArray.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
#include <pcl/filters/passthrough.h>

#include <random>

float noise_mean = 0;
float noise_sdev = 0;  // standard deviation

void filter_pc(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);

void velodyneCallback(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
    static ros::NodeHandle n("~");
    static ros::Publisher pub = n.advertise<sensor_msgs::PointCloud2>("velodyne_points", 0);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    sensor_msgs::PointCloud2::Ptr sm_pc2(new sensor_msgs::PointCloud2);
    pcl::fromROSMsg(*msg, *cloud);

    // Gaussian Noise Generator
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::normal_distribution<float> dist(noise_mean, noise_sdev);
    
    // for debug
    auto ogx = cloud->points[0].x;
    auto& nx = cloud->points[0].x;

    filter_pc(cloud);

    // Apply Gaussian Noise
    for (auto& point : cloud->points) 
    {
        point.x = point.x + dist(generator);
        point.y = point.y + dist(generator);
        point.z = point.z + dist(generator);
    }

    // Publish new Point Cloud
    pcl::toROSMsg(*cloud, *sm_pc2);
    pub.publish(sm_pc2);

    // debug msg
    //ROS_INFO_STREAM(nx-ogx);
}

void paramsCallback(const std_msgs::Float32MultiArray::Ptr& msg)
{
    /*
    if(msg->layout.dim.size != 2)
    {
        ROS_ERROR_STREAM("Received incorrect size float message: " << msg->layout.dim.size);
        return;
    }
    else
    {
        */
        ROS_INFO_STREAM("Changing params" << msg->data[0] << " " << msg->data[1]);
        noise_mean = msg->data[0];
        noise_sdev = msg->data[1];
    //}
}


void filter_pc(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
{
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(-10, 2);
    pass.setFilterLimitsNegative(true);
    pass.filter(*cloud);
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "noiser");
    ros::NodeHandle n("~");
    ros::Subscriber sub = n.subscribe("/velodyne_points", 0, velodyneCallback);
    ros::Subscriber sub_param = n.subscribe("params", 0, paramsCallback);
    n.getParam("mean", noise_mean);
    n.getParam("sdev", noise_sdev);
    ROS_INFO_STREAM("Running with Mean: " << noise_mean <<
                    " Standard Dev.: " << noise_sdev);

    ros::spin();
    return 0;
}