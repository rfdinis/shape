#include "my_logger.hpp"
#include <exception>

// ""macros""
static const bool FILTER_PC = false;     // whether to filter poincloud
static const double FILTER_UPPER = 2;    // upper limit of removed points
static const double FILTER_LOWER = -10;  // lower limit of removed points


/*
 * Subscribes to velodyne and sonar topics. 
 * Calls back with received_pointcloud and received_scan functions 
 */
void my_logger::sub()
{
    sub_velodyne = nh.subscribe("/velodyne_points", 0, &my_logger::received_pointcloud, this);
    sub_sonar    = nh.subscribe("/fw_asv0/sonar"  , 0, &my_logger::received_scan,       this);
}

/*
 * Unsubscribes sub_velodyne and sub_sonar
 */
void my_logger::unsub()
{
    sub_velodyne.shutdown();
    sub_sonar.shutdown();
}

void my_logger::set_dir(std::string d)
{
    int dir_err = mkdir(d.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if( dir_err == -1)
    {
        if( errno == EEXIST)  // directory already exists.
        {
            // delete files inside?
        }
        else
        {
            throw ros::Exception(
                    std::string("Error creating ") +
                    std::string(d) +
                    std::string(": ") +
                    std::strerror(errno)  // converts error code to something readable
                    );
        }
        
    }

    dir = d + "/";
}

/*
 * Converts PointCloud2 message to pcl pointcloud format.
 * Saves pointcloud to this->dir as a .pcd file
 * FILTER_PC flag to decide wether to remove values around z=0
 */
void my_logger::received_pointcloud(const sensor_msgs::PointCloud2 &msg)
{
    static int n = 0;
    pcl::PCLPointCloud2 pcl_pc2;
    n++;

    ROS_INFO_STREAM("Saving pc number " + std::to_string(n));
    pcl_conversions::toPCL(msg, pcl_pc2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromPCLPointCloud2(pcl_pc2, *cloud);

    if (FILTER_PC)
    {
        this->filter_pc(cloud);
    }

    pcl::io::savePCDFileASCII(this->dir + std::to_string(n) + ".pcd", *cloud);
}

void my_logger::received_scan(const sensor_msgs::LaserScan::Ptr& msg)
{
    //ROS_INFO_STREAM("woohoo i got a laser scan messsage");
    sensor_msgs::PointCloud2 cloud;
    laser_projector.transformLaserScanToPointCloud("fw_asv0::base_footprint", *msg, cloud, tfListener);
}

/*
 * Applies passthrough filter on pointcloud to remove values between z=FILTER_UPPER and z=FILTER_LOWER
 */
void my_logger::filter_pc(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
{
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(FILTER_LOWER, FILTER_UPPER);
    pass.setFilterLimitsNegative(true);
    pass.filter(*cloud);
}

/*
 * Acquires one pc message and saves it
 * times out at 10 seconds
 * returns true if the acquisiton was successful
 */
bool my_logger::get_one(const std::string& topic)
{
    try
    {
        auto pc  = ros::topic::waitForMessage<sensor_msgs::PointCloud2>(topic, ros::Duration(10));
        if(!pc) throw ros::Exception("waitForMessage timed out");
        this->received_pointcloud(*pc);
        return true;
    }
    catch(ros::Exception e)
    {
        ROS_ERROR_STREAM(e.what());
    }
    catch(std::exception s)
    {
        ROS_ERROR_STREAM(s.what());
    }
    catch(...)
    {
        ROS_ERROR_STREAM("Error getting point cloud.");
    }
    return false;
}
