#include "data_gatherer.hpp"

#include <random>
#include <assert.h>
#include <math.h>
#include <fstream>

data_gatherer::data_gatherer()
:base_dir("/tmp/")
{
}

void data_gatherer::gather(const turbs_t& vec)
{
    for(auto value: vec)
    {
        logger.set_dir( base_dir + std::get<2>(value) );
        gather_iter(std::get<0>(value), std::get<1>(value));
    }
}

void data_gatherer::gather_iter(double x, double y)
{
    for(auto& topic : topics)
    {
        gather_one(x,y, topic);
    }
    
}

void data_gatherer::gather_one(double x, double y, const std::string& topic)
{
    controller.teleport(x, y, 0);
    if( logger.get_one(topic) )
    {
        locations.push_back( point_t{x, y} );
    }
}

/*
 * Saves historic of locations where point clouds 
 * were gathered into a csv file
 */
void data_gatherer::locations_to_csv(std::string file)
{
    std::ofstream f(file);
    for( auto v: locations)
    {
        f << std::get<0>(v) << ",";
        f << std::get<1>(v) << std::endl;
    }
    f.close();
}

random_gatherer::random_gatherer()
:max_distance(30), min_distance(10), points_per_object(20)
{
}

/*
 * Randomly generates distance and angle to compute new position
 * Uniform distribution for each of the values
 */
void random_gatherer::gather_iter(double x, double y)
{
    std::random_device rd;   // To obtain a seed for the random number engine
    std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<double> 
                    distance(min_distance, max_distance),
                    angle(0, 2.0 * 3.1415926);

    for(int i = 0; i < points_per_object; i++)
    {
        double r     = distance(gen);
        double alpha = angle(gen);
        
        double dx    = r * sin(alpha);
        double dy    = r * cos(alpha);

        data_gatherer::gather_iter(x + dx, y + dy);
    }
}


ring_gatherer::ring_gatherer()
:num_rings(3), points_per_ring(360/20), min_radius(10), max_radius(30)
{
}

/*
 * Generates points in circles around the center x,y
 * Evenly spaced points
 */
void ring_gatherer::gather_iter(double x, double y)
{
    assert(max_radius > min_radius);
    assert(num_rings  > 1);

    double angle_step = 2 * 3.14159 / double(points_per_ring);
    for( int i = 0; i < num_rings; i++)  // loop over rings
    {
        double radius = (max_radius - min_radius) * i / (num_rings - 1) + min_radius;
        for( int j = 0; j < points_per_ring; j++ )  // loop over points in ring
        {
            double angle = angle_step * j; 
            double dx    = radius * cos(angle);
            double dy    = radius * sin(angle);
            data_gatherer::gather_iter(x + dx, y + dy);//gather_one(x + dx, y + dy);
        }
    }

}

/*
 * Real potato code bellow
 *  -base_dir gets changed and reverted to its original value at the end
 *  -logger.set_dir used to create temporary base_dir directories
 *  -hard coded noiser name
 *  -breaks consistency with rest of code
 * we got it all!
 */
void data_gatherer::split_noise_gather(const turbs_t &vec, std::vector<double> means, std::vector<double> sdevs)
{
    static noiser_controller noiser{"/noiser_t0/params"};
    std::string old_dir = base_dir;
    
    for(auto &mean: means)
    {
        for(auto &sdev: sdevs)
        {
            noiser.set_noise(mean, sdev);
            base_dir = old_dir + std::string("/mean") +
                       std::to_string(int(mean*10)) + 
                       std::string("_sdev") +
                       std::to_string(int(sdev*10)) + 
                       std::string("/");
            logger.set_dir(base_dir);
            gather(vec);
        }
    }
    base_dir = old_dir;
}
