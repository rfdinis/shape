#include "node_controller.hpp"

#include <geometry_msgs/Twist.h>
#include <std_msgs/Bool.h>
#include <gazebo_msgs/SetModelState.h>
#include <std_msgs/Float32MultiArray.h>

boat_controller::boat_controller()
:default_angular_speed(0.0), default_linear_speed(0.0)
{
  speed_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
  safety_pub = nh.advertise<std_msgs::Bool>("/safety_stop", 10);
}

/*
 * Teleports boat(fw_asv0) to x,y,z coordinates
 * Reference frame -> world
 * calls service /gazebo/set_model_state
 */
void boat_controller::teleport(double x = 0, double y = 0, double z = 0)
{
  geometry_msgs::Pose pose;
  pose.position.x = x;
  pose.position.y = y;
  pose.position.z = z;
  pose.orientation.x = 0.0;
  pose.orientation.y = 0.0;
  pose.orientation.z = 0.0;
  pose.orientation.w = 0.0;

  geometry_msgs::Twist twist;
  twist.linear.x = 0.0;
  twist.linear.y = 0.0;
  twist.linear.z = 0.0;
  twist.angular.x = 0.0;
  twist.angular.y = 0.0;
  twist.angular.z = 0.0;

  gazebo_msgs::ModelState modelstate;
  modelstate.model_name = "fw_asv0";
  modelstate.reference_frame = "world";
  modelstate.pose = pose;
  modelstate.twist = twist;

  ros::ServiceClient client = nh.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
  gazebo_msgs::SetModelState setmodelstate;
  setmodelstate.request.model_state = modelstate;
  client.call(setmodelstate);
  ROS_INFO_STREAM(std::string("Teleport to: (") + 
                  std::to_string(x) + std::string(", ") +
                  std::to_string(y) + std::string(", ") +
                  std::to_string(z) + std::string(")")
                  );
}

/*
 * Sets boat speed.
 * Boat will forget values after 1 second, so it is needed to send it repeatedly
 * Turns off the safety stop mode at every call
 */
void boat_controller::set_speed()
{
  geometry_msgs::Twist tw;
  tw.linear.x = default_linear_speed;
  tw.linear.y = 0;
  tw.linear.z = 0;
  tw.angular.x = 0;
  tw.angular.y = 0;
  tw.angular.z = default_angular_speed;

  std_msgs::Bool b;
  b.data = false;

  safety_pub.publish(b);
  speed_pub.publish(tw);
}

void boat_controller::set_speed(double linear, double angular)
{
  default_linear_speed = linear;
  default_angular_speed = angular;
  set_speed();
}

noiser_controller::noiser_controller(const char* name)
{
  param_pub = nh.advertise<std_msgs::Float32MultiArray>(name, 0);
}

void noiser_controller::set_noise(double mean, double sdev)
{

    ROS_INFO_STREAM("Sending new params: mean: " << mean <<  ", sdev: " << sdev);
    std_msgs::Float32MultiArray msg;
    msg.data.push_back(float(mean));
    msg.data.push_back(float(sdev));
    param_pub.publish(msg);
}
