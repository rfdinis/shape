#include <unistd.h>
#include <iostream>

#include <ros/ros.h>
#include "data_gatherer.hpp"

#define FAST_TEST 0
#define SPLIT_NOISE 1


int main(int argc, char **argv)
{
    ros::init(argc, argv, "logger");
    chdir("/home/rfdinis/Desktop/datasets/pcs4/");

    data_gatherer::turbs_t  
    TURBS{ // {X,Y,name}
        {0,    0,    "jacketX"},
        {300,  0,    "jacketK"},
        {0,    300,  "TJ0"},
        {300,  300,  "TJ30"},
        {600,  300,  "TJ60"},
        {-300, 300,  "TJ90"},
        {0,    -300, "tripodX"},
        {300,  -300, "tripodZ"},
        {300,  600,  "deepcwind"},
        {-300, 600,  "confloat"},
        {600,  600,  "windfloat"},
        {0,    -600, "monopile"}
    };

    data_gatherer::topics_t topics = {
        "/noiser_t0/velodyne_points"
    };

    ring_gatherer train;
    train.base_dir        = "train/";
    train.topics          = topics;
    train.max_radius      = 100;
    train.min_radius      = 20;
    train.num_rings       = (100-20)/5;
    train.points_per_ring = 360/20;  // steps of 20º

    random_gatherer test;
    test.base_dir          = "test/";
    test.topics            = topics;
    test.max_distance      = 100;
    test.min_distance      = 20;
    test.points_per_object = 100;

    #if FAST_TEST
    train.num_rings=2;
    train.points_per_ring=2;
    test.points_per_object=1;
    #endif
    
    
    std::vector<double> means{0, -2.5, 2.5};
    std::vector<double> test_sdevs{0.1, 0.7, 1.3, 2, 3};
    std::vector<double> train_sdevs{0.1, 0.5, 1, 1.5};
    
    train.split_noise_gather(TURBS, means, train_sdevs);
    test.split_noise_gather(TURBS, means, test_sdevs);
    
    train.locations_to_csv("train_locations.csv");
    test .locations_to_csv("test_locations.csv");
}
