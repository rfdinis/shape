#pragma once

#include <string>
#include <ros/ros.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl_ros/transforms.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <laser_geometry/laser_geometry.h>

class my_logger
{
public:
  my_logger(){};

  void sub();
  void unsub();
  void set_dir(std::string);
  bool get_one(const std::string& topic);

private:
  void received_pointcloud(const sensor_msgs::PointCloud2& msg);
  void received_scan(const sensor_msgs::LaserScan::Ptr& msg);
  void filter_pc(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);

  ros::Subscriber sub_velodyne, sub_sonar;
  ros::NodeHandle nh;
  tf::TransformListener tfListener;
  laser_geometry::LaserProjection laser_projector;
  std::string dir;
};
