#pragma once

// stl
#include <vector>
#include <string>
// mine
#include "my_logger.hpp"
#include "node_controller.hpp"


class data_gatherer{


public:
    data_gatherer();
    typedef std::vector<std::tuple<double, double, std::string>> turbs_t;
    typedef std::vector<std::string> topics_t;

    void gather(const turbs_t &vec);
    void locations_to_csv(std::string dir);

    // this function should die
    void split_noise_gather(const turbs_t &vec, 
             std::vector<double> means, 
             std::vector<double> sdevs); 

    std::string base_dir;  // must be an existing directory
    topics_t    topics;

protected:
    void gather_one(double x, double y, const std::string& topic);
    virtual void gather_iter(double center_x, double center_y);

    typedef std::tuple<double, double> point_t;
    typedef std::vector<point_t> locs_t;

private:
    my_logger logger;
    boat_controller controller;
    locs_t locations;

};


class random_gatherer : public data_gatherer{

public:
    random_gatherer();
    virtual void gather_iter(double center_x, double center_y);

    int points_per_object;
    double min_distance;
    double max_distance;

};


class ring_gatherer : public data_gatherer{

public:
    ring_gatherer();
    virtual void gather_iter(double center_x, double center_y);

    int num_rings;
    int points_per_ring;
    double min_radius;
    double max_radius;
    
};
