#pragma once

#include <ros/ros.h>

class boat_controller{

  public:
    boat_controller();
    void teleport(double x, double y, double z);

    void set_speed();
    void set_speed(double linear, double angular);

  private:
    ros::NodeHandle nh;
    ros::Publisher speed_pub;
    ros::Publisher safety_pub;

    double default_linear_speed, default_angular_speed;
};

class noiser_controller{

  public:
    noiser_controller(const char* name);
    void set_noise(double mean, double sdev);
  
  private:
    ros::NodeHandle nh;
    ros::Publisher param_pub;
};
