#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <shape_msgs/completion_request.h>

#include <pcl/filters/passthrough.h>

const double WAIT_FOR_MISSING = 30;
const std::string frame{"fw_asv0/base_footprint"};
const std::string velodyne_topic{"/velodyne_points"};
const std::string sonar_topic{"/fw_asv0/sonar"};


void filter_below(pcl::PointCloud<pcl::PointXYZ>::Ptr&, float);

int main(int argc, char** argv)
{
    ros::init(argc, argv, "demo");
    
    tf::TransformListener tfListener;//(ros::Duration(WAIT_FOR_MISSING));
    laser_geometry::LaserProjection projector;

    ros::NodeHandle nh;
    ros::Publisher pub   = nh.advertise<shape_msgs::completion_request>("shape_completion/request", 0);
    ros::Publisher upper = nh.advertise<sensor_msgs::PointCloud2>("test_up", 0, true);     // for rviz
    ros::Publisher laser = nh.advertise<sensor_msgs::PointCloud2>("test_laser", 0, true);  // for rviz
    ros::Publisher missn = nh.advertise<sensor_msgs::PointCloud2>("test_missing", 0, true);  // for rviz
    
    ros::Duration(3).sleep();
    while(ros::ok())
    {
        // await user input
        ROS_INFO_STREAM("Press enter to execute."); 
        getchar();

        // receive data to complete
        auto velodyne = ros::topic::waitForMessage<sensor_msgs::PointCloud2>(velodyne_topic, ros::Duration(5));
        auto sonar = ros::topic::waitForMessage<sensor_msgs::LaserScan>(sonar_topic, ros::Duration(1));
        if( velodyne==nullptr || sonar==nullptr )
        {
            ROS_ERROR_STREAM("Timed out waiting for message");
            continue;
        }

        // intermediate variables
        sensor_msgs::PointCloud2 transformed_cloud;
        pcl::PointCloud<pcl::PointXYZ>::Ptr sonar_cloud(new pcl::PointCloud<pcl::PointXYZ>), 
                                            velodyne_cloud(new pcl::PointCloud<pcl::PointXYZ>);

        // process velodyne cloud
        pcl_ros::transformPointCloud(frame, *velodyne, transformed_cloud, tfListener);
        pcl::fromROSMsg(transformed_cloud, *velodyne_cloud);
        filter_below(velodyne_cloud, 2);  // remove water reflection

        // process sonar cloud
        projector.transformLaserScanToPointCloud(frame, *sonar, transformed_cloud, tfListener);
        pcl::fromROSMsg(transformed_cloud, *sonar_cloud);
        filter_below(sonar_cloud, -40);  // remove sea bottom

        // send completion request
        shape_msgs::completion_request out;
        out.header.frame_id = frame;
        pcl::toROSMsg(*velodyne_cloud, out.velodyne);
        pcl::toROSMsg(*sonar_cloud, out.sonar);
        out.velodyne.header.stamp = velodyne->header.stamp;
        pub.publish(out);

        // for rviz
        upper.publish(out.velodyne);
        laser.publish(out.sonar);

        // completed cloud
        auto rep = ros::topic::waitForMessage<sensor_msgs::PointCloud2>("shape_completion/out", ros::Duration(WAIT_FOR_MISSING));
        if (rep!=nullptr) ROS_INFO_STREAM("Received");
        else ROS_ERROR_STREAM("Timed out");
    }

    return 0;
}


void filter_cloud(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, float min, float max)
{
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(min, max);
    pass.setFilterLimitsNegative(true);
    pass.filter(*cloud);
}

void filter_below(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, float highest)
{
    filter_cloud(cloud, -9999999999, highest);
}