#include <iostream>

#include <string>
#include <ros/ros.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl_ros/transforms.h>
#include <pcl/visualization/cloud_viewer.h>
#include <tf/transform_listener.h>
#include <pcl/filters/passthrough.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <laser_geometry/laser_geometry.h>
#include <shape_msgs/completion_request.h>


void filter_pc(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, float min, float max)
{
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(min, max);
    pass.setFilterLimitsNegative(true);
    pass.filter(*cloud);
}


class async_gather {
     public:
        async_gather(std::string, std::string);
        void laser_callback(const sensor_msgs::LaserScan::ConstPtr& scan);
        void velodyne_callback(const sensor_msgs::PointCloud2::ConstPtr& point);
        shape_msgs::completion_request get_msg();
        bool is_ready() { return has_both; }
        void clear(){ has_velodyne=false; has_both=false; }
     private:
        ros::NodeHandle nh;
        laser_geometry::LaserProjection projector;
        tf::TransformListener tfListener;

        ros::Subscriber laser_sub, velodyne_sub;
        std::string frame;
        
        bool has_velodyne;
        bool has_both;
        pcl::PointCloud<pcl::PointXYZ>::Ptr saved_velodyne;
        pcl::PointCloud<pcl::PointXYZ>::Ptr saved_laser;
};

async_gather::async_gather(std::string velodyne_topic, std::string sonar_topic)
:has_velodyne(false), has_both(false)
{
    velodyne_sub = nh.subscribe<sensor_msgs::PointCloud2> (velodyne_topic, 0, &async_gather::velodyne_callback, this);
    laser_sub = nh.subscribe<sensor_msgs::LaserScan> (sonar_topic, 0, &async_gather::laser_callback, this);
    saved_velodyne.reset(new pcl::PointCloud<pcl::PointXYZ>);
    saved_laser.reset(new pcl::PointCloud<pcl::PointXYZ>);
}

void async_gather::laser_callback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    if(!has_velodyne) return;
    sensor_msgs::PointCloud2 cloud;
    pcl::PCLPointCloud2 pcl_pc2;

    projector.transformLaserScanToPointCloud("fw_asv0/base_footprint", *scan, cloud, tfListener);
    pcl_conversions::toPCL(cloud, pcl_pc2);
    pcl::fromPCLPointCloud2(pcl_pc2, *saved_laser);
    filter_pc(saved_laser, -60, -40);
    has_both = true;

}

void async_gather::velodyne_callback(const sensor_msgs::PointCloud2::ConstPtr& cloud)
{
    if(has_both || has_velodyne) return;
    pcl::PCLPointCloud2 pcl_pc2;
    sensor_msgs::PointCloud2::Ptr tcloud(new sensor_msgs::PointCloud2);
    frame = cloud->header.frame_id;
    
    pcl_ros::transformPointCloud("fw_asv0/base_footprint", *cloud, *tcloud, tfListener);
    pcl_conversions::toPCL(*tcloud, pcl_pc2);
    pcl::fromPCLPointCloud2(pcl_pc2, *saved_velodyne);
    filter_pc(saved_velodyne, -20, 1.5);
    has_velodyne = true;
}

shape_msgs::completion_request async_gather::get_msg()
{
    shape_msgs::completion_request out;

    pcl::toROSMsg(*saved_velodyne, out.velodyne);
    pcl::toROSMsg(*saved_laser, out.sonar);

    out.header.frame_id = frame;
    out.velodyne.header.frame_id = frame;
    out.sonar.header.frame_id = frame;

    clear();
    return out;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "yee");
    async_gather gat("velodyne_points", "/fw_asv0/sonar");
    ros::NodeHandle nh;
    ros::Publisher pub   = nh.advertise<shape_msgs::completion_request>("shape_completion/request", 0);
    ros::Publisher upper = nh.advertise<sensor_msgs::PointCloud2>("test_up", 0, true);
    ros::Publisher laser = nh.advertise<sensor_msgs::PointCloud2>("test_laser", 0, true);

    while(ros::ok())
    {
        std::cout << "Press enter to execute.\n"; 
        getchar();
        
        ros::spinOnce(); gat.clear();
        while(!gat.is_ready()) { 
            ros::spinOnce(); 
            if(!ros::ok()) 
                return 1;
        }
        auto msg = gat.get_msg();
        msg.header.frame_id = "fw_asv0/base_footprint";
        
        msg.header.frame_id = msg.velodyne.header.frame_id;
        pub.publish(msg);  // for the program
        upper.publish(msg.velodyne);  // for rviz visualization
        laser.publish(msg.sonar);  // for rviz visualization

    }
    return 0;
}
