#pragma once
// pcl
#include <pcl/point_types.h>
#include <pcl/common/centroid.h>
#include <pcl/io/pcd_io.h>
// stl
#include <iostream>
#include <vector>
#include <string>

class registration
{
    using pc_t = pcl::PointCloud<pcl::PointXYZ>;

    public:
    registration();
    double evolve(pc_t::Ptr sensor_cloud, pc_t::Ptr template_cloud, pc_t::Ptr out);

    int       particles;    // pso particles
    int       iterations;   // pso iterations
    double    icp_it;       // max icp iterations
    double    w;            // intertia
    double    c1;           // local search gain
    double    c2;           // global search gain
    double    max_vel;      // [0..1]
    unsigned  var;          // https://esa.github.io/pagmo2/docs/cpp/algorithms/pso.html
    double    rp_lim;       // roll and pitch limit [-rp_lim, rp_lim]
    double    yaw_lim;      // particle yaw travels in [-yaw_lim, yaw_lim]
    double    z_lim;        // particle z travels in [-z_lim, z_lim]
    double    BB_low;       // Bounding box gain from template model on short end
    double    BB_high;      // Bounding box gain from template model on long end
    unsigned  verbosity;    // 0 -> mute all console output. 5 -> results shown every 5 iterations
};
