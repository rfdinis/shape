#pragma once

#include <pcl/point_types.h>
#include <pcl/pcl_base.h>

using pc_t = pcl::PointCloud<pcl::PointXYZ>;

void view_2PCs(pc_t::Ptr&, pc_t::Ptr&);  // red and green
void view_nPCs(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>&vec); 
int get_closest_point(const pc_t::Ptr& pointcloud);  // returns index
void get_pc_dims(const pc_t::Ptr, double& width_x, double& width_y);

double fit_icp(pc_t::Ptr target, pc_t::Ptr source, pc_t::Ptr out, double max_it);
double fit_icp_reverse(pc_t::Ptr target, pc_t::Ptr source, pc_t::Ptr out, double max_it);
void scale_pc(const pc_t::Ptr& source, pc_t::Ptr& output, float scale);
void filter_pc(pc_t::Ptr &cloud, float min, float max, bool negative=true);
void subtract_pc(pc_t::Ptr& full, pc_t::Ptr& sub, pc_t::Ptr& out, double radius);

Eigen::Matrix4f get_transform_matrix(std::vector<double> dof);
Eigen::Matrix4f get_transform_matrix(std::vector<float> dof);
void transform_6dof(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, std::vector<double> dof);
void transform_6dof(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, std::vector<float> dof);

void rotate_pc(const pc_t::Ptr& source, pc_t::Ptr& output, float r, float p, float y);
void rotate_raw_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float r, float p, float y);
void rotate_reverse_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float r, float p, float y);
void translation_pc(const pc_t::Ptr& source, pc_t::Ptr& output, float dx, float dy, float dz);
void translation_reverse_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float dx, float dy, float dz);

