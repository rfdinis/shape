#pragma once

#include <pagmo/problem.hpp>
#include <pcl/pcl_base.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>

namespace pagmo{

class icp_var {

public:
    
    using cloud = pcl::PointCloud<pcl::PointXYZ>;
    using cloud_ptr = cloud::Ptr;

    icp_var();
    icp_var(cloud_ptr& target, cloud_ptr& source, vector_double lower_bound, vector_double upper_bound, double icp_iterations);
    // Fitness computation
    virtual vector_double fitness(const vector_double &) const;
    // Box-bounds
    std::pair<vector_double, vector_double> get_bounds() const;
    /// Problem name
    std::string get_name() const{ return "Multistart ICP"; }
    std::vector<vector_double> get_history();
    void save_history(std::string file);

protected:
    cloud_ptr target;
    cloud_ptr source;

    vector_double lower_bound;
    vector_double upper_bound;
    double icp_it;
};

class icp_optim: public icp_var {

public:
    
    using cloud = pcl::PointCloud<pcl::PointXYZ>;
    using cloud_ptr = cloud::Ptr;

    icp_optim();
    icp_optim(cloud_ptr& target, cloud_ptr& source, vector_double lower_bound, vector_double upper_bound, double icp_iterations);
    // Fitness computation
    vector_double fitness(const vector_double &) const;

private:
};

}
