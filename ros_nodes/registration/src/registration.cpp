// pagmo
#include <pagmo/problem.hpp>
#include <pagmo/algorithms/pso.hpp>
#include <pagmo/population.hpp>
// self
#include "icp_optim.hpp"
#include "pc_utils.hpp"
#include "registration.hpp"


registration::registration()
{
    particles  =  7;   // pso particles
    iterations = 20;   // pso iterations
    icp_it     = 1;    // max icp iterations
    w          = .79;  // intertia
    c1         = 2.0;  // local search gain
    c2         = 1.5;  // global search gain
    max_vel    = .07;  // [0..1]
    var        = 5;    // https://esa.github.io/pagmo2/docs/cpp/algorithms/pso.html
    rp_lim     = .1;   // roll and pitch limit
    yaw_lim    = 3.14;
    z_lim      = 20;
    verbosity  = iterations/10 + 1;
}

double registration::evolve(pc_t::Ptr sensor_cloud, pc_t::Ptr template_cloud, pc_t::Ptr out)
{
    float center_x;
    float center_y;

    pcl::PointXYZ cent; 
    pcl::computeCentroid(*sensor_cloud, cent);
    center_x = cent.x;
    center_y = cent.y;

    // Define X and Y Bounds
    double width_x, width_y;
    get_pc_dims(template_cloud, width_x, width_y);
    float min_x = center_x > 0 ? center_x-(width_x/4) : center_x-(width_x/2);
    float min_y = center_y > 0 ? center_y-(width_y/4) : center_y-(width_y/2);
    float max_x = center_x > 0 ? center_x+(width_x/2) : center_x+(width_x/4);
    float max_y = center_y > 0 ? center_y+(width_y/2) : center_y+(width_y/4);

    // Print Bounds
    if(verbosity)
    {
        std::cout << "X Limits: " << min_x << ": " << max_x << std::endl;
        std::cout << "Y Limits: " << min_y << ": " << max_y << std::endl;
    }

    // PSO
    pagmo::vector_double ub {max_x, max_y,  z_lim,  rp_lim,   rp_lim,  yaw_lim};  // upper bound
    pagmo::vector_double lb {min_x, min_y, -z_lim, -rp_lim,  -rp_lim, -yaw_lim};  // lower bound
    pagmo::icp_optim udp{template_cloud, sensor_cloud, ub, lb, icp_it};  // user defined problem
    pagmo::problem prob{udp};
    pagmo::population pop{prob, particles, 4};
    pagmo::pso alg{iterations, w, c1, c2, max_vel, var};
    
    alg.set_verbosity(verbosity);
    alg.set_seed(4u);
    
    // Solve problem and get output
    pop = alg.evolve(pop);
    auto dof = pop.champion_x();

    for (int i = 0; i < dof.size() && verbosity; i++) 
        std::cout << "xyzrpy"[i] << ": " << dof[i] << std::endl;  // PSO result

    //return 0;
    transform_6dof(template_cloud, out, dof);
    auto score = fit_icp_reverse(out,sensor_cloud, out, icp_it);
    udp.save_history("/home/rfdinis/Desktop/pso_hist.csv");
    return score;
}

/*
void complete_shape(pc_t::Ptr sensor_cloud, pc_t::Ptr template_cloud, pc_t::Ptr missing)
{
    int point_idx = get_closest_point(sensor_cloud);
    float closest_x = sensor_cloud->at(point_idx).x;
    float closest_y = sensor_cloud->at(point_idx).y;
    double width_x, width_y;

    get_pc_dims(template_cloud, width_x, width_y);
    float min_x = closest_x > 0 ? closest_x-(width_x/2) : closest_x-width_x;
    float min_y = closest_y > 0 ? closest_y-(width_y/2) : closest_y-width_y;
    float max_x = closest_x > 0 ? closest_x+width_x : closest_x+(width_x/2);
    float max_y = closest_y > 0 ? closest_y+width_y : closest_y+(width_y/2);

    if(verbosity)
    {
        std::cout << "Closest Point: " << sensor_cloud->at(point_idx) << std::endl;
        std::cout << "X Limits: " << min_x  << ": " << max_x << std::endl;
        std::cout << "Y Limits: " << min_y << ": " << max_y << std::endl;
    }

    pagmo::vector_double ub {max_x, max_y,   50,  rp_lim,   rp_lim,  3.14};  // upper bound
    pagmo::vector_double lb {min_x, min_y,  -50, -rp_lim,  -rp_lim, -3.14};  // lower bound
    pagmo::problem prob{pagmo::icp_var{template_cloud, sensor_cloud, ub, lb, icp_it}};
    pagmo::population pop{prob, particles};
    pagmo::pso alg{iterations, w, c1, c2, max_vel, var};
    alg.set_verbosity(verbosity);
    alg.set_seed(4u);

    pop = alg.evolve(pop);
    auto dof = pop.champion_x();
    for (int i = 0; i < dof.size(); i++)
        std::cout << "xyzrpy"[i] << ": " << dof[i] << std::endl;

    pc_t::Ptr temp(new pc_t);
    transform_6dof(template_cloud, temp, dof);
    fit_icp_reverse(temp,sensor_cloud, temp, icp_it);
    subtract_pc(temp, sensor_cloud, missing, subtraction_radius);

}
*/