#include "pc_utils.hpp"
#include <cstdlib>
#include <iostream>
#include <pcl/filters/voxel_grid.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/passthrough.h>
#include <pcl/visualization/point_cloud_handlers.h>
#include <pcl/common/impl/io.hpp>
#include <pcl/common/centroid.h>
#include <pcl/common/colors.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/registration/icp.h>
#include <pcl/features/moment_of_inertia_estimation.h>

#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

#define USE_PCL_VISUALIZER 1

using pc_t = pcl::PointCloud<pcl::PointXYZ>;
using rgb_pc_t = pcl::PointCloud<pcl::PointXYZRGB>;

double fit_icp(pc_t::Ptr target, pc_t::Ptr source, pc_t::Ptr out, double max_it=10000)
{
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    pcl::PointCloud<pcl::PointXYZ> Final;

    icp.setMaximumIterations(max_it);
    icp.setInputSource(source);
    icp.setInputTarget(target);
    icp.align(Final);
    
    auto tf = icp.getFinalTransformation();
    pcl::transformPointCloud(*source, *out, tf);

    return icp.getFitnessScore();
}

// Performs regular ICP, but instad of applying the transform on the source cloud, applies it on the target cloud
double fit_icp_reverse(pc_t::Ptr target, pc_t::Ptr source, pc_t::Ptr out, double max_it=10000)
{
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    pcl::PointCloud<pcl::PointXYZ> Final;

    icp.setMaximumIterations(max_it);
    icp.setInputSource(source);
    icp.setInputTarget(target);
    icp.align(Final);
    
    auto tf = icp.getFinalTransformation();
    pcl::transformPointCloud(*target, *out, tf.inverse());
    return icp.getFitnessScore();
}


void filter_pc(pc_t::Ptr &cloud, float min, float max, bool negative)
{
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(min, max);
    pass.setFilterLimitsNegative(negative);
    pass.filter(*cloud);
}

void view_2PCs(pc_t::Ptr& p1, pc_t::Ptr& p2)
{
    unsigned int darker = 60;
    rgb_pc_t::Ptr rgb1 (new rgb_pc_t);
    rgb_pc_t::Ptr rgb2 (new rgb_pc_t);
    copyPointCloud(*p1, *rgb1);
    copyPointCloud(*p2, *rgb2);
    for(auto& point: rgb1->points){point.r =   0; point.g = 255-darker; point.b = 0; }
    for(auto& point: rgb2->points){point.r = 255-darker; point.g =   0; point.b = 0; }

    #if USE_PCL_VISUALIZER
    pcl::visualization::PCLVisualizer viewer("3D Viewer");
    viewer.addPointCloud(rgb1, "1");
    viewer.addPointCloud(rgb2, "2");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 8, "1");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, "2");
    viewer.setBackgroundColor(180,180,180);
    viewer.spin();
    #else
    pcl::visualization::CloudViewer viewer("3D Viewer");
    viewer.showCloud(rgb1, "1");
    viewer.showCloud(rgb2, "2");
    while (!viewer.wasStopped ())
    {
    }
    #endif
}

void view_nPCs(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>&vec)
{
    pcl::GlasbeyLUT colors; 
    pcl::visualization::PCLVisualizer viewer("3D Viewer");
    int i = 0;
    for (auto& pc : vec)
    {
        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> rgb (pc, colors.at(i).r, 
                                                                            colors.at(i).g, 
                                                                            colors.at(i).b);
        viewer.addPointCloud(pc, rgb, std::to_string(i));
        viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 
                                                3, std::to_string(i));
        i++;
    }
    viewer.spin();
}


void scale_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float scale)
{
    Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
    transform (0,0) = scale;
    transform (1,1) = scale;
    transform (2,2) = scale;
    pcl::transformPointCloud (*source_cloud, *output, transform); 
}

Eigen::Matrix4f get_transform_matrix(std::vector<double> dof)
{
    std::vector<float> temp;
    for( double v: dof)
        temp.push_back(float(v));
    return get_transform_matrix(temp);
}

Eigen::Matrix4f get_transform_matrix(std::vector<float> dof)
{
    Eigen::Matrix4f transform_x = Eigen::Matrix4f::Identity();
    transform_x (1,1) =  std::cos (dof[3]);
    transform_x (1,2) = -std::sin (dof[3]);
    transform_x (2,1) =  std::sin (dof[3]);
    transform_x (2,2) =  std::cos (dof[3]);

    Eigen::Matrix4f transform_y = Eigen::Matrix4f::Identity();
    transform_y (0,0) =  std::cos (dof[4]);
    transform_y (2,0) = -std::sin (dof[4]);
    transform_y (0,2) =  std::sin (dof[4]);
    transform_y (2,2) =  std::cos (dof[4]);

    Eigen::Matrix4f transform_z = Eigen::Matrix4f::Identity();
    transform_z (0,0) =  std::cos (dof[5]);
    transform_z (0,1) = -std::sin (dof[5]);
    transform_z (1,0) =  std::sin (dof[5]);
    transform_z (1,1) =  std::cos (dof[5]);

    Eigen::Matrix4f transform = transform_z * transform_y * transform_x;
    transform (0,3) = dof[0];
    transform (1,3) = dof[1];
    transform (2,3) = dof[2];

    return transform;
}

void transform_6dof(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, std::vector<float> dof)
{
    pcl::PointXYZ cent;
    pcl::computeCentroid(*source_cloud, cent);
    translation_pc(source_cloud, output, -cent.x, -cent.y, -cent.z);

    auto tf = get_transform_matrix(dof);
    pcl::transformPointCloud (*output, *output, tf);

    translation_pc(output, output, cent.x, cent.y, cent.z);
}

void transform_6dof(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, std::vector<double> dof)
{
    transform_6dof(source_cloud, output, std::vector<float>
                                        {float(dof[0]), float(dof[1]), float(dof[2]), 
                                         float(dof[3]), float(dof[4]), float(dof[5])});
}

void rotate_raw_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float r, float p, float y)
{
    transform_6dof(source_cloud, output, std::vector<float>{0,0,0,r,p,y});
}

void rotate_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float r, float p, float y)
{
    // https://en.wikipedia.org/wiki/Rotation_matrix#General_rotations
    // r - roll, p - pitch, y - yaw
    // radians

    // move to origin
    pcl::PointXYZ cent;
    pcl::computeCentroid(*source_cloud, cent);
    translation_pc(source_cloud, output, -cent.x, -cent.y, -cent.z);
    
    rotate_raw_pc(source_cloud, output, r, p , y);

    // restore original position
    translation_pc(output, output, cent.x, cent.y, cent.z);
}

void rotate_reverse_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float r, float p, float y)
{
    pcl::PointXYZ cent;
    pcl::computeCentroid(*source_cloud, cent);
    translation_pc(source_cloud, output, -cent.x, -cent.y, -cent.z);

    auto transform = get_transform_matrix(std::vector<float>{0,0,0,r,p,y});
    transformPointCloud (*source_cloud, *output, transform.inverse());

    translation_pc(source_cloud, output, cent.x, cent.y, cent.z);
}


void translation_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float dx, float dy, float dz)
{
    // https://en.wikipedia.org/wiki/Translation_(geometry)
    auto transform = get_transform_matrix(std::vector<float>{dx,dy,dz,0,0,0});
    transformPointCloud (*source_cloud, *output, transform);
}

void translation_reverse_pc(const pc_t::Ptr& source_cloud, pc_t::Ptr& output, float dx, float dy, float dz)
{
    // https://en.wikipedia.org/wiki/Translation_(geometry)
    auto transform = get_transform_matrix(std::vector<float>{dx,dy,dz,0,0,0});
    transformPointCloud (*source_cloud, *output, transform.inverse());
}

void subtract_pc(pc_t::Ptr& full_cloud, pc_t::Ptr& subtracting_cloud, pc_t::Ptr& out, double radius=1)
{
    *out = *full_cloud;  // copy full_cloud to output before delet
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud(out);
    extract.setNegative(true);

    pcl::KdTreeFLANN<pcl::PointXYZ> tree;
    for( auto& point : *subtracting_cloud)
    {
        std::vector<int> indices;
        std::vector<float> sqr_distances;

        tree.setInputCloud(out);  // this is required in order to update the tree
        tree.radiusSearch(point, radius, indices, sqr_distances);

        pcl::PointIndices::Ptr pc_indices(new pcl::PointIndices);
        pc_indices->indices = indices;  // pcl is weird
        
        extract.setIndices(pc_indices);
        extract.filter(*out);
    }
    // remove outliers
    /*
    pcl::RadiusOutlierRemoval<pcl::PointXYZ> outrem;
    outrem.setInputCloud(out);
    outrem.setRadiusSearch(1);
    outrem.setMinNeighborsInRadius (20);
    outrem.filter (*out);
    */
}

int get_closest_point(const pc_t::Ptr& pointcloud)
{
    std::vector<int> k_indices;
    std::vector<float> k_sqr_distances;
    
    pcl::KdTreeFLANN<pcl::PointXYZ> tree;
    pcl::PointXYZ origin(0,0,0);
    tree.setInputCloud(pointcloud);
    tree.nearestKSearch(origin, 1, k_indices, k_sqr_distances);

    return k_indices[0];
}


void get_pc_dims(const pc_t::Ptr cl, double& width_x, double& width_y)
{
    pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;
    feature_extractor.setInputCloud (cl);
    feature_extractor.compute ();
    pcl::PointXYZ min_point_AABB;  // AABB = axis aligned bounding box
    pcl::PointXYZ max_point_AABB;
    feature_extractor.getAABB(min_point_AABB, max_point_AABB);
    
    width_x = max_point_AABB.x - min_point_AABB.x;
    width_y = max_point_AABB.y - min_point_AABB.y;

}
