#include <pagmo/problem.hpp>
#include <pcl/registration/icp.h>
#include <fstream>
#include "pc_utils.hpp"
#include "icp_optim.hpp"

using namespace pagmo;

static std::vector<vector_double> history;  // has to be global because of const

icp_var::icp_var()
{
    auto cloud1 = cloud_ptr(new cloud);
    auto cloud2 = cloud_ptr(new cloud);
    icp_var(cloud1, cloud2, {2,  2, 20,  0.5,  0.5,  1.57}, {-2, -2,  0, -0.5, -0.5, -1.57}, 100);
}

icp_var::icp_var(cloud_ptr& target, cloud_ptr& source, 
                 vector_double upper_bound, 
                 vector_double lower_bound, 
                 double icp_it )
{
    this->target = target;
    this->source = source;
    this->lower_bound = lower_bound;
    this->upper_bound = upper_bound;
    this->icp_it = icp_it;
    history.clear();
}

vector_double icp_var::fitness(const vector_double &dof) const
{
    cloud_ptr temp(new cloud);
    transform_6dof(target, temp, dof);
    auto fit = fit_icp(temp, source, temp, icp_it);
    history.push_back({dof[0], dof[1], dof[2], dof[3], dof[4], dof[5], fit});

    return {fit};
}

std::pair<vector_double, vector_double> icp_var::get_bounds() const
{
    return {lower_bound, upper_bound};
}

std::vector<vector_double> icp_var::get_history()
{
    return history;
}

void icp_var::save_history(std::string fname)
{
    std::ofstream f(fname);
    for( auto item : history )
    {
        for( auto coord : item ) f << coord << ",";
        f << std::endl;
    }
}


icp_optim::icp_optim()
:icp_var()
{
}

icp_optim::icp_optim(cloud_ptr& target, cloud_ptr& source, 
                    vector_double upper_bound, 
                    vector_double lower_bound, 
                    double icp_it )
:icp_var(target, source, upper_bound, lower_bound, icp_it)
{
}

vector_double icp_optim::fitness(const vector_double &dof) const
{
    cloud_ptr temp(new cloud);
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    icp.setInputSource(source);
    icp.setInputTarget(target);
    icp.setMaximumIterations(icp_it);

    Eigen::Matrix4f guess = get_transform_matrix(dof);
    icp.align(*temp, guess.inverse());

    auto fit = icp.getFitnessScore();
    history.push_back({dof[0], dof[1], dof[2], dof[3], dof[4], dof[5], fit});

    return {fit};
}
