#include <pcl/point_types.h>
#include <pcl/common/centroid.h>
#include <pcl/io/pcd_io.h>
#include <iostream>
#include <vector>
#include <string>

#include "pc_utils.hpp"
#include "registration.hpp"

#define WITH_ROS 1
#if WITH_ROS
    #include <ros/ros.h>
    #include <sensor_msgs/PointCloud2.h>
    #include <std_msgs/String.h>
    #include <pcl_conversions/pcl_conversions.h>
    #include <shape_msgs/classified_incomplete.h>
    #include <shape_msgs/registration_output.h>

    const std::string OUT_TOPIC{"shape_completion/missing_points"};
    const std::string  IN_TOPIC{"shape_completion/req_cmp"}; 
    using output_msg = shape_msgs::registration_output; //sensor_msgs::PointCloud2;
    using input_msg = shape_msgs::classified_incomplete;
    using input_param = const input_msg::ConstPtr;
#else
    #include <chrono>
#endif

double subtraction_radius = 0.7;
using pc_t = pcl::PointCloud<pcl::PointXYZ>;

void load_template(std::string type, pc_t::Ptr& cloud, bool high_res=false)
{
    if (high_res)
        pcl::io::loadPCDFile("/home/rfdinis/catkin_ws/src/shape/ros_nodes/registration/template_10k/" + type + ".pcd", *cloud);
    else
        pcl::io::loadPCDFile("/home/rfdinis/catkin_ws/src/shape/ros_nodes/registration/template/" + type + ".pcd", *cloud);

    scale_pc(cloud, cloud, 0.001f);          // fixes stl model scale
    rotate_pc(cloud, cloud, M_PI/2, 0, 0);   // fixes stl model orientation
    pcl::PointXYZ cent; 
    pcl::computeCentroid(*cloud, cent);
    translation_pc(cloud, cloud, -cent.x, -cent.y, -cent.z);
}

void load_template_fixed(std::string type, pc_t::Ptr& cloud)
{
    pcl::io::loadPCDFile("/home/rfdinis/catkin_ws/src/shape/ros_nodes/registration/template_10k_fixed/" + type + ".pcd", *cloud);
}

#if WITH_ROS
void completion_callback(registration reg, input_param& msg, output_msg& reply)
{
    pc_t::Ptr template_cloud(new pc_t),
              sensor_cloud(new pc_t), 
              missing(new pc_t), 
              temp(new pc_t);

    pcl::fromROSMsg(msg->upper, *sensor_cloud);
    pcl::fromROSMsg(msg->lower, *temp);
    *sensor_cloud += *temp;

    //load_template(msg->prediction, template_cloud, true);
    load_template_fixed(msg->prediction, template_cloud);
    double score = reg.evolve(sensor_cloud, template_cloud, missing);
    subtract_pc(missing, sensor_cloud, missing, subtraction_radius);

    pcl::toROSMsg(*missing,reply.missing_points);
    reply.fitness = score;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "registration");
    ros::NodeHandle nh;
    ros::Publisher pub = nh.advertise<output_msg>(OUT_TOPIC, 0);

    registration reg;
    reg.iterations = 20;
    reg.particles  = 7;
    reg.verbosity  = 4;

    auto callback = [pub, reg](input_param msg)
                    {output_msg reply; completion_callback(reg, msg, reply);  pub.publish(reply);};
    ros::Subscriber sub = nh.subscribe<input_msg>(IN_TOPIC, 1, callback);
    ros::spin();
    return 0;
}

#else
int main (int argc, char** argv)
{      
    pc_t::Ptr sensor_cloud(new pc_t);
    pc_t::Ptr template_cloud(new pc_t);
    pc_t::Ptr out(new pc_t);

    load_template("jacketX", template_cloud, true);
    pcl::io::loadPCDFile("/home/rfdinis/catkin_ws/src/shape/ros_nodes/registration/models/sample1.pcd", *sensor_cloud);

    filter_pc(sensor_cloud,   -60, -40);  // remove bottom
    transform_6dof(sensor_cloud, sensor_cloud, std::vector<float>{50,50,0,0,0,3.14/4});

    registration reg;
    reg.verbosity=0;
    reg.iterations=10;

    std::vector<int> particles_it{2,5,7,10,15,20,50};
    for(auto &p : particles_it)
    {   
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        reg.particles = p;
        auto fit = reg.evolve(sensor_cloud, template_cloud, out);
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

        std::cout << "Particles: " << p << std::endl
                  << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "ms" << std::endl
                  << "Fitness: " << fit << std::endl << std::endl;
                
    }
    return 0;
}
#endif

