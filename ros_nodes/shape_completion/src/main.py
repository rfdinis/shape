#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import PointCloud2
from shape_msgs.msg import completion_request, classified_shape, classification_request, classified_incomplete, registration_output
import sensor_msgs
import time

rospy.init_node('shape_completion')

pub_seg = rospy.Publisher('/shape_completion/req_seg', PointCloud2, queue_size=0)
pub_cls = rospy.Publisher('/shape_completion/req_cls', classification_request, queue_size=0)
pub_mis = rospy.Publisher('/shape_completion/req_cmp', classified_incomplete, queue_size=0)
pub_out = rospy.Publisher('/shape_completion/out', PointCloud2, queue_size=0)

class global_state:
    def __init__(self):
        self.lock = False
        self.sonar = None
        self.cloud = None
        self.frame = None
        self.timer = None
    def clear(self):
        self.__init__()

state = global_state()


def receive_raw_pc(msg):
    global state
    if not state.lock:
        rospy.loginfo('Received Completion Request')
        pub_seg.publish(msg.velodyne)
        state.lock  = True
        state.sonar = msg.sonar
        state.frame = msg.velodyne.header
        state.timer = time.time()


def received_segmented_pc(msg):
    global state

    if len(msg.data) == 0:
        rospy.loginfo('No structure detected by clustering')
        pub_out.publish(msg)
        state.clear()
    else:
        rospy.loginfo('Received Segmented Cloud, %d points' % len(msg.data))
        nm = classification_request()
        nm.cloud = msg
        pub_cls.publish(nm)
        state.cloud = msg


def received_cls_pred(msg):
    rospy.loginfo('Received Classification Prediction')

    prediction = msg.prediction
    score = msg.score

    nm = classified_incomplete()
    nm.prediction = prediction
    nm.upper = state.cloud
    nm.lower = state.sonar
    
    pub_mis.publish(nm)


def received_missing_points(msg):
    global state
    rospy.loginfo('Received Missing Data')
    rospy.loginfo(f'Fitness Score: {msg.fitness}')

    missing = msg.missing_points
    missing.header = state.frame
    pub_out.publish(missing)

    rospy.loginfo(f'Total time: {time.time() - state.timer}')
    state.clear()

sub_raw = rospy.Subscriber('/shape_completion/request', completion_request, receive_raw_pc, queue_size=1)
sub_seg = rospy.Subscriber('/shape_completion/segmented_points', PointCloud2, received_segmented_pc, queue_size=1)
sub_cls = rospy.Subscriber('/shape_completion/prediction', classified_shape, received_cls_pred, queue_size=1)
sub_mis = rospy.Subscriber('/shape_completion/missing_points', registration_output, received_missing_points, queue_size=1)

rospy.spin()
