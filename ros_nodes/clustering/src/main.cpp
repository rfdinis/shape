#include <iostream>
#include <string>
#include <unistd.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/colors.h>

#define RUN_TEST 0

#if RUN_TEST
    #define VIEW_CLUSTERED_PCS 1
#else
    #define VIEW_CLUSTERED_PCS 0
    #include <ros/ros.h>
    #include <sensor_msgs/PointCloud2.h>
    #include <pcl_conversions/pcl_conversions.h>
    const std::string  IN_TOPIC{"/shape_completion/req_seg"};
    const std::string OUT_TOPIC{"/shape_completion/segmented_points"};
#endif

using namespace pcl;

void view_nPCs(std::vector<PointCloud<PointXYZ>::Ptr> vec)
{
    #if VIEW_CLUSTERED_PCS
    pcl::GlasbeyLUT colors; 
    pcl::visualization::PCLVisualizer viewer("3D Viewer");
    int i = 0;
    for (auto& pc : vec)
    {
        visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> rgb (pc, colors.at(i).r, 
                                                                            colors.at(i).g, 
                                                                            colors.at(i).b);
        viewer.addPointCloud(pc, rgb, std::to_string(i));
        viewer.setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_POINT_SIZE, 3, std::to_string(i));
        i++;
    }
    viewer.spin();
    #endif
}



bool get_turbine_cluster(const PointCloud<PointXYZ>::Ptr& cloud, PointCloud<PointXYZ>::Ptr& out, float min_height, float tolerance)
{
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new search::KdTree<PointXYZ>);
    tree->setInputCloud (cloud);

    std::vector<PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (tolerance);
    ec.setMinClusterSize (100);
    ec.setMaxClusterSize (5000);
    ec.setSearchMethod (tree);
    ec.setInputCloud (cloud);
    ec.extract (cluster_indices);
    
    if(cluster_indices.size() == 0)
        return false;

    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> cache;
    std::vector<double> centroid_z;

    for (auto& it : cluster_indices)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
        for (auto& pit : it.indices)
            cloud_cluster->points.push_back (cloud->points[pit]);
        
        cloud_cluster->width = cloud_cluster->points.size ();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;

        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*cloud_cluster,centroid); 
        std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points.\nCentroid: " << centroid.transpose().block(0,0,1,3) << std::endl << std::endl;
        
        if (centroid(2,0) > min_height)
        {
            centroid_z.push_back(centroid(2,0));
            cache.push_back(cloud_cluster);
        }
    }

    view_nPCs(cache);

    // get result based on max centroid z
    auto max = std::max_element(centroid_z.begin(), centroid_z.end());
    auto max_idx = std::distance(centroid_z.begin(), max);
    if(*max > min_height)
    {
        *out = *cache[max_idx];
        return true;
    }

    return false;
}

void scale_pc(const PointCloud<PointXYZ>::Ptr& source_cloud, PointCloud<PointXYZ>::Ptr& output, float scale)
{
    Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
    transform (0,0) = scale;
    transform (1,1) = scale;
    transform (2,2) = scale;
    transformPointCloud (*source_cloud, *output, transform); 
}

void move_pc(const PointCloud<PointXYZ>::Ptr& source_cloud, PointCloud<PointXYZ>::Ptr& output, float x)
{
    Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
    transform (0,3) = x;
    transform (1,3) = x;
    transform (2,3) = 0;
    transformPointCloud (*source_cloud, *output, transform); 
}

#if RUN_TEST
int main(int argc, char** argv) {
    chdir("/home/rfdinis/catkin_ws/src/shape/ros_nodes/clustering/src/");
    std::string cloud_file = "windfloat.pcd";
    std::string obstacle_file = "tj.pcd";
    
    PointCloud<PointXYZ>::Ptr cloud (new PointCloud<PointXYZ>), 
                              obstacle(new PointCloud<PointXYZ>),  
                              turb(new PointCloud<PointXYZ>);      // output
    io::loadPCDFile(cloud_file, *cloud);
    io::loadPCDFile(obstacle_file, *obstacle);
    
    scale_pc(obstacle, obstacle, 1.0/10.0);
    *cloud = *cloud + *obstacle;
    move_pc(obstacle, obstacle, -30);
    *cloud = *cloud + *obstacle;
    move_pc(obstacle, obstacle, 10);
    *cloud = *cloud + *obstacle;

    view_nPCs({cloud});

    get_turbine_cluster(cloud, cloud, 0.1, 20);
    get_turbine_cluster(cloud, cloud, 0.1, 10);
    view_nPCs({cloud});

}
#else

using input_msg = sensor_msgs::PointCloud2;
using output_msg = sensor_msgs::PointCloud2;

using input_param = const input_msg::ConstPtr&;
void callback_fn(input_param msg, ros::Publisher replier)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    sensor_msgs::PointCloud2 out;

    pcl::fromROSMsg(*msg, *cloud);
    //get_turbine_cluster(cloud, cloud, 0.1, 20);
    //get_turbine_cluster(cloud, cloud, 0.1, 10);
    pcl::toROSMsg(*cloud, out);
    
    replier.publish(out);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "clusterer");
    ros::NodeHandle nh;
    ros::Publisher replier = nh.advertise<output_msg>(OUT_TOPIC, 0);
    auto callback = [replier](input_param msg){callback_fn(msg, replier);};
    ros::Subscriber sub = nh.subscribe<input_msg>(IN_TOPIC, 0, callback);
    ros::spin();
    return 0;
} 

#endif