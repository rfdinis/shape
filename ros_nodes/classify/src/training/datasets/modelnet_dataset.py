
import numpy as np
import warnings
import os
from torch.utils.data import Dataset
from pyntcloud import PyntCloud
import pandas as pd
warnings.filterwarnings('ignore')

def voxel_converter(cloud, vox_n=24):
    """
    converts pyntcloud to voxel grid of shape vox_n*vox_n*vox_n
    output is padded to a grid of 32*32*32
    """
    cloud = pd.DataFrame(cloud, columns=['x', 'y', 'z'])
    cloud = PyntCloud(cloud)

    voxelgrid_id = cloud.add_structure("voxelgrid", n_x=vox_n, n_y=vox_n, n_z=vox_n)
    grid = cloud.structures[voxelgrid_id].get_feature_vector()

    pad = int((32-vox_n)/2)
    pad_m = ((pad,pad), (pad,pad), (pad,pad))
    grid = np.pad(grid, pad_m, 'constant')

    return grid

class modelnet_dataset(Dataset):
    def __init__(self, root,  npoint=1024, split='train', cache_size=15000, transform=None):
        self.root = root
        self.npoints = npoint
        self.catfile = os.path.join(self.root, 'modelnet40_shape_names.txt')
        self.transform = transform

        self.cat = [line.rstrip() for line in open(self.catfile)]
        self.classes = dict(zip(self.cat, range(len(self.cat))))
        #self.labels = 

        shape_ids = {}
        shape_ids['train'] = [line.rstrip() for line in open(os.path.join(self.root, 'modelnet40_train.txt'))]
        shape_ids['test'] = [line.rstrip() for line in open(os.path.join(self.root, 'modelnet40_test.txt'))]

        assert (split == 'train' or split == 'test')
        shape_names = ['_'.join(x.split('_')[0:-1]) for x in shape_ids[split]]
    
        # list of (shape_name, shape_txt_file_path) tuple
        self.datapath = [(shape_names[i], os.path.join(self.root, shape_names[i], shape_ids[split][i]) + '.txt') for i
                         in range(len(shape_ids[split]))]

        self.cache_size = cache_size  # how many data points to cache in memory
        self.cache = {}  # from index to (point_set, cls) tuple

    def __len__(self):
        return len(self.datapath)

    def _get_item(self, index):
        if index in self.cache:
            point_set, target = self.cache[index]
        else:
            fn = self.datapath[index]
            target = self.classes[self.datapath[index][0]]
            target = np.array([target]).astype(np.int32)
            point_set = np.loadtxt(fn[1], delimiter=',').astype(np.float32)
            point_set = point_set[0:self.npoints,0:3]

            if len(self.cache) < self.cache_size:
                self.cache[index] = (point_set, target)
            
        sample = self.transform(point_set[:,0:3]) if self.transform is not None else point_set[:,0:3]
        grid = voxel_converter(sample)
        return np.expand_dims(grid, axis=0), target

    def __getitem__(self, index):
        return self._get_item(index)




if __name__ == '__main__':
    import torch
    from utils import plot_voxel_grid
    import matplotlib.pyplot as plt
    from transforms import random_point_sample
    tf = random_point_sample(200,400)
    data = modelnet_dataset('/home/rfdinis/Desktop/modelnet40_normal_resampled',split='train', transform=tf)
    DataLoader = torch.utils.data.DataLoader(data, batch_size=1, shuffle=False)
    for point,label in DataLoader:
        plot_voxel_grid(point[0])
        plt.title(data.cat[label])
        plt.show()