import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pyntcloud import PyntCloud
import parse

import os
import os.path
import sys

import torchvision


def read_bin(path):

    names = ['t','intensity','id',
             'x','y','z',
             'azimuth','range','pid']

    formats = ['int64', 'uint8', 'uint8',
               'float32', 'float32', 'float32',
               'float32', 'float32', 'int32']

    binType = np.dtype( dict(names=names, formats=formats) )
    data = np.fromfile(path, binType)
    # 3D points, one per row
    P = np.vstack([ data['x'], data['y'], data['z'] ]).T
    
    df = pd.DataFrame(P)
    df.columns=['x','y','z']
    
    return PyntCloud(df)
    

def voxel_converter(cloud, vox_n=32):
    """
    converts pyntcloud to voxel grid of shape vox_n*vox_n*vox_n
    output is padded to a grid of 32*32*32
    """
    voxelgrid_id = cloud.add_structure("voxelgrid", n_x=vox_n, n_y=vox_n, n_z=vox_n)
    grid = cloud.structures[voxelgrid_id].get_feature_vector()

    pad = int((32-vox_n)/2)
    pad_m = ((pad,pad), (pad,pad), (pad,pad))
    grid = np.pad(grid, pad_m, 'constant')

    return grid


def make_dataset(dir, classes, extensions='.bin'):
    pcs = []
    dir = os.path.expanduser(dir)
    def is_valid_file(x):
        return x.lower().endswith(extensions)

    for f in os.listdir(dir):
        if is_valid_file(f):
            params = parse.parse('{}.{}.{}.bin', f)
            target = params[0]
            if target in classes:
                item = (dir+f, target)
                pcs.append(item)
    return pcs


class sydney_dataset(torchvision.datasets.VisionDataset):
    '''
    4wd             21
    bench           2
    bicycle         8
    biker           4
    building        20
    bus             16
    car             88
    cyclist         3
    excavator       1
    pedestrian      152
    pillar          20
    pole            21
    post            8
    scooter         1
    ticket_machine  3
    traffic_lights  47
    traffic_sign    51
    trailer         1
    trash           5
    tree            34
    truck           12
    trunk           55
    umbrella        5
    ute             16
    van             35
    vegetation      2
    '''
    def __init__(self, root, extensions='.bin', transform=None, is_valid_file=None):
        super(sydney_dataset, self).__init__(root, transform=transform)
        
        classes = ['4wd','bench','bicycle','biker','building','bus','car','cyclist','excavator','pedestrian','pillar','pole','post','scooter' ,'ticket_machine','traffic_lights',
                    'traffic_sign','trailer','trash','tree','truck','trunk','umbrella','ute' ,'van','vegetation']

        samples = make_dataset(self.root, classes, extensions)
        if len(samples) == 0:
            raise (RuntimeError("Found 0 files in subfolders of: " + self.root + "\n"
                                "Supported extensions are: " + ",".join(extensions)))

        self.loader = read_bin
        self.extensions = extensions

        self.classes = classes
        self.samples = samples
        self.targets = [s[1] for s in samples]

    def __getitem__(self, index):
            """
            Args:
                index (int): Index
            Returns:
                tuple: (sample, target) where target is class_index of the target class.
            """
            path, target = self.samples[index]
            sample = self.loader(path)
            if self.transform is not None: 
                sample.xyz = self.transform(sample.xyz)
     
            return np.expand_dims(voxel_converter(sample), axis=0), self.classes.index(target)


    def __len__(self):
        return len(self.samples)

if __name__ == '__main__':
    import utils
    import matplotlib.pyplot as plt

    a = sydney_dataset('~/pCloudDrive/sydney_dset/objects/')
    s = a[50]
    
    print(s)
    print(a.classes[s[1]])

    utils.plot_voxel_grid(s[0])
    #plt.show()