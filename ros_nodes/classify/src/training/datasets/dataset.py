import os
from torchvision.datasets import DatasetFolder
from pyntcloud import PyntCloud
import numpy as np
import pandas as pd
from math import ceil


def voxel_converter(points, vox_n=24):
    """
    converts pyntcloud to voxel grid of shape vox_n*vox_n*vox_n
    output is padded to a grid of 32*32*32
    """
    if type(points) is not PyntCloud:
        cloud = pd.DataFrame(points, columns=['x','y','z'])
        cloud = PyntCloud(cloud)
    else: cloud = points

    voxelgrid_id = cloud.add_structure("voxelgrid", n_x=vox_n, n_y=vox_n, n_z=vox_n)
    grid = cloud.structures[voxelgrid_id].get_feature_vector()

    pad = int((32-vox_n)/2)
    pad_m = ((pad,pad), (pad,pad), (pad,pad))
    grid = np.pad(grid, pad_m, 'constant')

    return grid


def cloud_resample(cloud, npoints=100):
    length = cloud.shape[0]
    new_cloud = cloud
    if length < npoints:
        tiles     = ceil(npoints/length)
        new_cloud = np.tile(new_cloud, (tiles,1))  # tile it npoints/length time, (39,3) -> (117,3)
        new_cloud = new_cloud[:npoints,:]          # remove extra points
    elif length > npoints:
        idx       = np.random.randint(length, size=npoints)
        new_cloud = new_cloud[idx,:]
    return new_cloud


def cloud_normalize(cloud):
    centroid = np.mean(cloud, axis=0)
    cloud = cloud - centroid
    m = np.max(np.sqrt(np.sum(cloud**2, axis=1)))
    cloud = cloud / m
    return cloud


class PointDataset(DatasetFolder):
    def __init__(self, root, transform=None, cloud_size=100):
        super(PointDataset, self).__init__(
            root, loader=PyntCloud.from_file, 
            extensions='.pcd', 
            transform=transform,
            )
        self.data = {}
        self.cloud_size = cloud_size
    
    def __getitem__(self, index):
        path, target = self.samples[index]
        sample = self.loader(path)

        points = cloud_normalize(sample.xyz)
        if self.cloud_size is not None:
            points = cloud_resample(points)
        if self.transform is not None:
            points = self.transform(points)
        
        return points.T.astype(np.float64), target


class VoxelDataset(PointDataset):
    def __init__(self, root, voxel_transform=None, transform=None):
        super(VoxelDataset,self).__init__(
            root=root, 
            transform=transform,
            cloud_size=None
            )
        self.voxel_transform=voxel_transform
    
    def __getitem__(self, index):
        path, target = self.samples[index]
        sample = self.data[path] if self.preload else self.loader(path)
        
        if self.transform is not None:
            sample.xyz = self.transform(sample.xyz)

        grid = voxel_converter(sample, vox_n=24)

        if self.voxel_transform is not None:
            grid = self.voxel_transform(grid)

        return np.expand_dims(grid, axis=0), target


if __name__=='__main__':
    from torch.utils.data import DataLoader
    dataset = VoxelDataset(root="/home/rfdinis/pCloudDrive/datasets/pcs3/train/mean0_sdev1")
    dl = DataLoader(dataset, batch_size=1, shuffle=False)

    a = dataset[91][0]
    print(a.shape)
    print('dataset size: ', len(dataset))

