import os
from collections import namedtuple
import numpy as np
import torch
import tqdm
from torch.utils.data import DataLoader
import models

class test_arguments:
    def __init__(self):
        self.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        self.batch_size = 1
        self.loss_criterion = torch.nn.CrossEntropyLoss()

def test(model, dataset, args=test_arguments()):
    dataloader = DataLoader(dataset, batch_size=args.batch_size, shuffle=False)
    criterion = args.loss_criterion
    device = args.device

    model.to(device)
    model.eval()

    loss = 0; errors = 0; predictions = []
    confusion_matrix = np.zeros((len(dataset.classes), len(dataset.classes)))
    Pred = namedtuple('NN_Output', ['ID', 'Prediction', 'Target', 'Score', 'Target_Score', 'File'])

    # Test loop
    with torch.no_grad():
        softmax = torch.nn.Softmax()
        loop = tqdm.tqdm(dataloader, leave=False)
        for i, (data, target) in enumerate(loop):
            # Run batch
            data   = data.to(device).double()
            target = target.to(device).long().flatten()
            prediction = model(data)

            # Info
            p = softmax(prediction)
            prediction_choice = p.max(1)[1]
            prediction_score = p.max(1)[0]
            data_source = dataset.samples[i][0]
            target_score = p[0][target]

            # Output/stats
            loss += criterion(prediction, target).item()
            predictions.append(Pred(i, prediction_choice.item(), target.item(), prediction_score.item(), target_score.item(), data_source))
            confusion_matrix[target, prediction_choice] += 1
            if prediction_choice != target: errors += 1
            accuracy = ((i+1) - errors)/(i+1)

            # Update progress bar info
            loop.set_description('[Acc=%.3f]' % (accuracy) )

    return accuracy, loss/len(dataset), predictions, confusion_matrix


if __name__ == '__main__':
    from parse import parse
    from datasets.dataset import PointDataset, VoxelDataset
    from torchvision.transforms import Compose
    from torch.utils.data import ConcatDataset
    import utils
    import models
    import matplotlib.pyplot as plt
    import pandas as pd

    WORK_DIR = '/home/rfdinis/pCloudDrive/datasets/pcs3/'
    PREDICTIONS_CSV = 'results/predictions.csv'
    RESULTS_CSV = 'results/results.csv'
    MODEL_FILE = 'results/model_checkpoint.torch'
    torch.manual_seed(4)
    os.chdir(WORK_DIR)
    assert(os.path.exists('results'))

    model = torch.load(MODEL_FILE)
    results = pd.DataFrame()
    predictions = pd.DataFrame()
    cms = []
    for d in os.listdir('test/')[:4]:
        mean, sdev = [float(x)/10 for x in parse('mean{}_sdev{}', d)]
        
        dset = PointDataset('test/' + d)
        acc,loss,preds,cm = test(model, dset)

        precision, recall = utils.scores_from_confusion_matrix(cm)
        cms.append(cm)
        results = results.append(
            pd.DataFrame({
                'root':      [d],
                'length':    [len(dset)],
                'mean':      [mean],
                'sdev':      [sdev],
                'loss':      [loss],
                'accuracy':  [acc],
                'precision': [precision],
                'recall':    [recall],
                })
            )
        preds_df = pd.DataFrame(preds, columns=preds[0]._fields)
        preds_df['mean'] = mean
        preds_df['sdev'] = sdev
        predictions = predictions.append(preds_df)


    predictions = predictions.sort_values(by=['mean','sdev'])
    results = results.sort_values(by=['mean', 'sdev'])
    predictions.to_csv(PREDICTIONS_CSV)
    results.to_csv(RESULTS_CSV)
