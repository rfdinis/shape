from datasets.sydney_dataset import sydney_dataset
from datasets.modelnet_dataset import modelnet_dataset
from train_classify import *
import utils
import matplotlib.pyplot as plt
import models
import pandas as pd
from transforms import random_noise, random_point_sample
from torchvision.transforms import Compose

torch.manual_seed(4)
device = 'cuda:0'
os.chdir('/home/rfdinis/pCloudDrive/datasets/pcs3/')
print('Running on: ', device)

tf = Compose([
    #random_point_sample(150,400),
    #random_noise(0.1),
    #random_rotator(3.14,axis=(0,0,1))
])

#dset = sydney_dataset('~/pCloudDrive/datasets/sydney_dset/objects/', transform=random_noise(1.0,0.1))
dset = modelnet_dataset('/home/rfdinis/Desktop/modelnet40_normal_resampled',split='train', transform=random_point_sample(150,400))
testset = modelnet_dataset('/home/rfdinis/Desktop/modelnet40_normal_resampled',split='test', transform=random_point_sample(150,400))

num_classes = len(dset.classes)
model = models.voxnet(len(dset.classes)).double().to(device)
args = train_arguments()
args.epochs = 20
args.batch_size = 128
args.optimizer = torch.optim.Adam(model.parameters())
#model = torch.load('pretrain_out/pretrained_model.torch')
history = train(model, dset, args)
torch.save(model,'pretrain_out/pretrained_model_lightnet.torch')

acc,_,res,cm = test(model, testset)
prec, rec = utils.scores_from_confusion_matrix(cm)
print('Accuracy: %.03f\nPrecision: %.03f\nRecall: %.03f' % (acc,prec,rec))

df = pd.DataFrame(res)
df.to_csv('pretrain_out/results.csv')

df = pd.DataFrame(history)
df.to_csv('pretrain_out/hist_lightnet.csv')
