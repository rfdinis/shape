import torch
import tqdm
from torch.utils.data import DataLoader
from utils import early_stopping
from test_classifier import test

class train_arguments():
    def __init__(self):
        self.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        self.batch_size = 32
        self.epochs = 100
        self.loss_criterion = torch.nn.CrossEntropyLoss()
        self.optimizer = None
        # learning rate decay
        self.decay_steps = 10
        self.decay_gamma = 0.5
        # early stopping
        self.es_patience = 3  # how many times it can not-improve
        self.es_delta = 0  # minimum improvement
        self.es_checkpoint_file = 'model_checkpoint.torch'
        self.es_validation_frequency = 2  # validations per epoch
        self.es_validation_dataset = None


def train(model, trainset, args: train_arguments):

    # Return Struct
    history = {'loss': [], 'accuracy': [], 'validation_accuracy': [], 'validation_loss':[], 'learning_rate':[]}

    # Training setup
    device     = args.device
    num_epoch  = args.epochs
    batch_size = args.batch_size
    optimizer  = args.optimizer
    criterion  = args.loss_criterion
    scheduler  = torch.optim.lr_scheduler.StepLR(optimizer, args.decay_steps, args.decay_gamma)
    model.to(device)
    trainloader = DataLoader(trainset, batch_size=batch_size, shuffle=True)

    # Early stopping setup
    validation_frequency = args.es_validation_frequency
    valset = args.es_validation_dataset
    checkpoint_name = args.es_checkpoint_file
    validation_accuracy = 0.0
    validation_loss = None
    validation_counter = 0
    early_stop = early_stopping(patience=args.es_patience, delta=args.es_delta, checkpoint_name=checkpoint_name)

    for epoch in range(num_epoch):
        batch_loop = tqdm.tqdm(trainloader, leave=False)
        for data, target in batch_loop:

            model.train()
            data   = data.to(device).double()
            target = target.to(device).long().flatten()
            optimizer.zero_grad()
            current_batch_size = data.size()[0]

            prediction = model(data).view(current_batch_size, -1)
            loss = criterion(prediction, target)
            loss.backward()
            optimizer.step()

            prediction_choice = prediction.max(1)[1]
            correct = (prediction_choice == target).sum().item()
            accuracy = correct/current_batch_size

            history['loss'].append(loss.item())
            history['accuracy'].append(accuracy)
            history['validation_loss'].append(validation_loss)
            history['validation_accuracy'].append(validation_accuracy)
            history['learning_rate'].append(scheduler.get_lr())

            batch_loop.set_description( '[Ep:%d/%d][ES:%d/%d] Validation=%0.3f | Accuracy=%0.3f' % (
                epoch+1, num_epoch, 
                early_stop.counter, early_stop.patience, 
                validation_accuracy, accuracy
                )
            )

            # Early stopping
            validation_counter += batch_size
            if validation_frequency is not None and validation_counter >= len(trainset)/validation_frequency:
                validation_counter = 0
                validation_accuracy, validation_loss,_,_ = test(model, valset)
                if early_stop(validation_loss, model):
                    print('Early stopping at epoch %d.' % (epoch))
                    model = torch.load(checkpoint_name)
                    return history
        scheduler.step()
    return history


if __name__ == '__main__':
    import os
    import pandas as pd
    from torchvision.transforms import Compose
    from torch.utils.data import ConcatDataset
    
    import utils
    import models
    from transforms import random_rotator, shuffle_points, random_scale
    from datasets.dataset import PointDataset, VoxelDataset

    WORK_DIR = '/home/rfdinis/pCloudDrive/datasets/pcs3/'
    TRAIN_DIRS = ['train/mean0_sdev1','train/mean0_sdev5', 'train/mean0_sdev15']
    VAL_DIR = 'test/mean0_sdev13/'
    TRAIN_HIST_CSV = 'results/train_history.csv'
    MODEL_CHECKPOINT = 'results/model_checkpoint.torch'
    MODEL_FILE = './model2.torch'
    
    NUM_CLASSES = 12
    ENABLE_TRANSFER = False
    torch.manual_seed(4)
    os.chdir(WORK_DIR)
    if not os.path.exists('results'): os.makedirs('results')
    else: print('Results folder already exists, may overwrite data...')

    # Model
    if ENABLE_TRANSFER:
        model = torch.load('pretrain_out/pretrained_model.torch').double()
        model.fc4 = torch.nn.Linear(128, NUM_CLASSES).double()  # reset last layer
    else:
        model = models.pointnet(NUM_CLASSES, feature_transform=True).double()

    # Augmentation
    cloud_tf = Compose([
        shuffle_points(),
        random_scale(0.8,1.2),
        random_rotator(3.14,  axis=(0,0,1)),  # yaw
    ])

    # Datasets
    trainset = ConcatDataset([ PointDataset(d, transform=cloud_tf) for d in TRAIN_DIRS ])
    valset = PointDataset(VAL_DIR)

    # Arguments
    args = train_arguments()
    model = model.to(args.device)
    args.optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
    args.decay_steps = 20
    args.decay_gamma = 0.5
    args.batch_size = 32
    args.loss_criterion = torch.nn.NLLLoss()
    args.es_patience=5000
    args.es_validation_frequency=2
    args.es_checkpoint_file = MODEL_CHECKPOINT
    args.es_validation_dataset = valset
    
    # Train
    history = train(model, trainset, args)
    
    # Save Results
    torch.save(model.cpu(), MODEL_FILE)
    train_history = pd.DataFrame.from_dict(history, orient='index').transpose()
    train_history.to_csv(TRAIN_HIST_CSV)

