import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import seaborn as sns
import numpy as np
import itertools
import torch

class early_stopping:
    def __init__(self, patience=5, delta=0, checkpoint_name=None, enable=True):
        self.enable          = enable
        self.delta           = delta
        self.patience        = patience
        self.checkpoint_name = checkpoint_name

        self.best_score = None
        self.counter    = 0

    def __call__(self, loss, model=None):
        return self.check(loss, model)

    def check(self, loss, model=None):
        score = -loss  # loss bad

        if self.best_score is None:
            self.best_score=score
            self._save_checkpoint(model)
            return False

        if score < self.best_score + self.delta:
            self.counter += 1
        else:
            self.counter = 0
            self.best_score = score
            self._save_checkpoint(model)

        return self.counter > self.patience

    def _save_checkpoint(self, model):
        if self.checkpoint_name is None:
            return
        if model is None:
            return
        try:
            torch.save(model, self.checkpoint_name)
        except UserWarning:  # pytorch bug
            pass

def scores_from_confusion_matrix(cm):
    np.seterr(divide='ignore', invalid='ignore')
    recall = np.diag(cm) / cm.sum(axis=1)
    precision = np.diag(cm) / cm.sum(axis=0)
    return precision.mean(), recall.mean()

def plot_confusion_matrix(cm, classes=None, normalize=False):

    if classes == None:
        classes = range(cm.shape[0])
    
    mat = cm.copy()
    if normalize:
        fmt = '.3f'
        for n, line in enumerate(cm):
            sum = line.sum()
            mat[n,:] = line/sum
    else:
        fmt='g'

    ax = plt.subplot()
    sns.heatmap(mat, annot=True, ax = ax, cmap='Blues', fmt=fmt) #annot=True to annotate cells

    # labels, title and ticks
    ax.set_title('Confusion Matrix')
    ax.set_xlabel('Predicted labels')
    ax.set_ylabel('True labels')
    ax.xaxis.set_ticklabels(classes)
    ax.yaxis.set_ticklabels(classes)

    # first and last get cut off without this line
    ax.set_ylim(0, len(classes))
    return ax


def plot_voxel_grid(vox):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    if len(vox.shape) == 4:
        ax.voxels(vox[0])
    else:
        ax.voxels(vox)
    return fig


def plot_two_voxel_grids(vox1,vox2):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.voxels(vox1, color='blue')
    ax.voxels(vox2, color='red')
    return fig


def plot_error(testset, errors, n):
    e = errors[n]
    res = '\n'.join((
        'ID:    %d' % (e.ID), 
        'Pred:  %s' % (testset.classes[e.Prediction]),
        'Label: %s' % (testset.classes[e.Target]),
        '\n'
        ))
    print(res)
    return plot_voxel_grid(testset[e.ID][0][0])


def plot_hist(values, labels, title='', xlab='', ylab=''):
    assert(len(values)==len(labels))
    x = np.arange(len(values))
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)

    plt.bar(x, values)
    plt.xticks(x, labels)

if __name__ == '__main__':
    data = np.random.rand(3, 3, 3)
    #plot_confusion_matrix(data, ['aaaa','b','c'])
    # plot_voxel_grid(data>0.5)