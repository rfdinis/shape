from math import cos,sin
import numpy as np

def R(theta, u):
    """
    Returns rotation matrix for axis u=(x,y,z), angle=theta
    """
    return np.array([
            [cos(theta) + u[0]**2 * (1-cos(theta)), 
             u[0] * u[1] * (1-cos(theta)) - u[2] * sin(theta), 
             u[0] * u[2] * (1 - cos(theta)) + u[1] * sin(theta)],
            
            [u[0] * u[1] * (1-cos(theta)) + u[2] * sin(theta),
             cos(theta) + u[1]**2 * (1-cos(theta)),
             u[1] * u[2] * (1 - cos(theta)) - u[0] * sin(theta)],

            [u[0] * u[2] * (1-cos(theta)) - u[1] * sin(theta),
             u[1] * u[2] * (1-cos(theta)) + u[0] * sin(theta),
             cos(theta) + u[2]**2 * (1-cos(theta))]
            ])

class random_rotator:
    """
    Rotates point cloud in range [-limit, limit]
    axis = (roll, pitch, yaw), default=(0,0,1)
    """
    def __init__(self, limit, axis=(0,0,1)):
        self.limit = limit
        self.axis  = axis

    def __call__(self, cloud):
        theta = np.random.rand()*2*self.limit - self.limit
        w = R(theta, self.axis)
        return cloud @ w

class shuffle_points:    
    """ 
    Shuffle orders of points in each point cloud.
    Use the same shuffling idx for the entire batch.
    """
    def __call__(self, data):
        idx = np.arange(data.shape[0])
        np.random.shuffle(idx)
        return data[idx,:]

class random_scale:    
    """ 
    Randomly scale the point cloud. Scale is per point cloud.
    """
    def __init__(self, scale_low, scale_high):
        self.low = scale_low
        self.high = scale_high
    def __call__(self, data):
        scale = np.random.uniform(self.low, self.high)
        data *= scale
        return data

class random_noise:
    """
    Adds white noise to point cloud
    Noise STD is randomized each iteration
    """
    def __init__(self, max_std, min_std=0):
        self.max_std = max_std
        self.min_std = min_std

    def __call__(self, cloud):
        std = np.random.uniform(self.min_std, self.max_std)
        noise = np.random.normal(0, std, cloud.shape)
        return cloud + noise

class random_sample:
    def __init__(self, min_points, max_points):
        self.min = min_points
        self.max = max_points
    
    def __call__(self, cloud):
        n_points = np.random.randint(low=self.min, high=self.max)
        idx = np.random.randint(cloud.shape[0], size=n_points)
        return cloud[idx,:]


if __name__ == '__main__':
    pass