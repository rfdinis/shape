import torch
import torch.nn as nn


class voxnet(nn.Module):
    def __init__(self, num_classes):
        super(voxnet, self).__init__()
        self.conv1  = nn.Conv3d(1, 32, stride=2, kernel_size=5)
        self.lrelu1 = nn.LeakyReLU()
        self.drop1  = nn.Dropout(p=0.3)
        self.conv2  = nn.Conv3d(32, 32, stride=1, kernel_size=3)
        self.lrelu2 = nn.LeakyReLU()
        self.pool2  = nn.MaxPool3d(2)
        self.drop2  = nn.Dropout(p=0.2)
        self.fc3    = nn.Linear(32*6*6*6, 128)
        self.relu3  = nn.ReLU()
        self.fc4    = nn.Linear(128, num_classes)
        
    def forward(self, x):
        x = self.lrelu1(self.conv1(x))
        x = self.drop1(x)
        x = self.pool2(self.lrelu2(self.conv2(x)))
        x = self.drop2(x)
        x = x.view(-1, 32*6*6*6)
        x = self.relu3(self.fc3(x))
        x = self.fc4(x)
        return x

class custom_voxnet(nn.Module):
    def __init__(self, num_classes):
        super(custom_voxnet, self).__init__()
        self.conv1  = nn.Conv3d(1, 32, stride=2, kernel_size=5)
        self.lrelu1 = nn.LeakyReLU()
        self.drop1  = nn.Dropout(p=0.2)

        self.conv2  = nn.Conv3d(32, 32, stride=1, kernel_size=3)
        self.lrelu2 = nn.LeakyReLU()
        self.pool2  = nn.MaxPool3d(2)
        self.drop2  = nn.Dropout(p=0.3)
        
        self.fc3    = nn.Linear(32*6*6*6, 128)
        self.relu3  = nn.ReLU()
        self.fc4    = nn.Linear(128, num_classes)

        self.bn1    = nn.BatchNorm3d(32)
        self.bn2    = nn.BatchNorm3d(32)
        self.bn3    = nn.BatchNorm1d(128)

    def forward(self, x):
        x = self.lrelu1(self.conv1(x))
        x = self.bn1(x)
        x = self.drop1(x)

        x = self.lrelu2(self.conv2(x))
        x = self.bn2(x)
        x = self.pool2(x)
        x = self.drop2(x)

        x = x.view(-1, 32*6*6*6)
        x = self.fc3(x)
        x = self.bn3(x)
        x = self.relu3(x)
        x = self.fc4(x)

        return x