import torch
import torch.nn as nn
import torch.nn.functional as F


class linear_component(nn.Module):
    def __init__(self, x, y, z, num_classes):
        super(linear_component, self).__init__()
        self.fc1 = nn.Linear(x*y*z*128, 128).double()
        self.fc2 = nn.Linear(128, num_classes).double()
    
    def forward(self, x):
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)
        x = F.softmax(x)
        return x


class lightnet_linear(nn.Module):
    def __init__(self, num_classes):
        super(lightnet_linear, self).__init__()
        self.num_classes = num_classes
        self.main = linear_component(2,2,2,num_classes)
        self.aux = [linear_component(1,1,1,num_classes) for _ in range(8)]

    def forward(self, x):
        xmain = x.reshape(-1,128*8)
        xmain = self.main(xmain)

        x1 = self.aux[0](x[:,:,0,0,0]).view(-1,self.num_classes,1)
        x2 = self.aux[1](x[:,:,0,0,1]).view(-1,self.num_classes,1)
        x3 = self.aux[2](x[:,:,0,1,0]).view(-1,self.num_classes,1)
        x4 = self.aux[3](x[:,:,0,1,1]).view(-1,self.num_classes,1)
        x5 = self.aux[4](x[:,:,1,0,0]).view(-1,self.num_classes,1)
        x6 = self.aux[5](x[:,:,1,0,1]).view(-1,self.num_classes,1)
        x7 = self.aux[6](x[:,:,1,1,0]).view(-1,self.num_classes,1)
        x8 = self.aux[7](x[:,:,1,1,1]).view(-1,self.num_classes,1)
        xaux = torch.cat((x1,x2,x3,x4,x5,x6,x7,x8), dim=2)
        xaux = torch.mean(xaux, dim=2)

        xo   = xmain*0.5 + xaux*0.5
        return xo

class lightnet_cnn(nn.Module):
    def __init__(self):
        super(lightnet_cnn, self).__init__()

        self.conv1 = nn.Conv3d(1, 32, kernel_size=5, stride=2)
        self.conv2 = nn.Conv3d(32, 32, kernel_size=3, stride=1)
        self.conv3 = nn.Conv3d(32, 128, kernel_size=3, stride=1)

        self.pool2 = nn.MaxPool3d(2)
        self.pool3 = nn.MaxPool3d(2)

        self.drop1 = nn.Dropout(p=0.3)
        self.drop2 = nn.Dropout(p=0.2)
        self.drop3 = nn.Dropout(p=0.1)

        self.bn1   = nn.BatchNorm3d(32)
        self.bn2   = nn.BatchNorm3d(32)
        self.bn3   = nn.BatchNorm3d(128)

    def forward(self, x):

        x = self.conv1(x)
        x = F.leaky_relu(x)

        x = self.conv2(x)
        x = F.leaky_relu(x)
        x = self.bn2(x)
        x = self.pool2(x)
        x = self.drop2(x)

        x = self.conv3(x)
        x = F.leaky_relu(x)
        x = self.bn3(x)
        x = self.pool3(x)
        x = self.drop3(x)

        return x


class lightnet(nn.Module):
    def __init__(self, num_classes):
        super(lightnet, self).__init__()
        self.num_classes = num_classes
        self.conv   = lightnet_cnn()
        self.linear = lightnet_linear(num_classes)

    def forward(self, x):
        x = self.conv(x)
        x = self.linear(x)
        return x


class lightnet_simple(nn.Module):
    def __init__(self, num_classes):
        super(lightnet, self).__init__()
        self.conv = lightnet_cnn()
        
        self.fcm   = nn.Linear(2*2*2*128, 128)
        self.out   = nn.Linear(128, num_classes)

    def forward(self, x):
        x = self.conv(x)
        x = x.view(-1, 2*2*2*128)

        x = self.fcm(x)
        x = F.relu(x)
        x = self.out(x)

        return x

