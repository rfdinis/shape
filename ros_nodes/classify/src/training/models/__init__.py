from .voxnet import voxnet, custom_voxnet
from .lightnet import lightnet
from .pointnet import PointNetCls as pointnet