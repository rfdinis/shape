import torch
import numpy as np
import os
import sys
os.chdir(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('training/')  # to import the model
import models
from datasets.dataset import VoxelDataset, PointDataset
from transforms import random_rotator

device='cpu'
classes = ['TJ0', 'TJ30', 'TJ60', 'TJ90', 'confloat', 'deepcwind', 'jacketK', 'jacketX', 'monopile', 'tripodX', 'tripodZ', 'windfloat']
tf = random_rotator(3.14)
d = '/home/rfdinis/pCloudDrive/datasets/pcs3/test/mean0_sdev13'
def classify(model, sample):
    sample = np.expand_dims(sample,axis=0)
    sample = torch.from_numpy(sample)
    prediction = model(sample).max(1)
    return classes[prediction[1]]

if False:  # voxnet
    model = torch.load('voxnet.torch', map_location=device).eval()
    dset_rot = VoxelDataset(d, transform=tf)
    dset_normal = VoxelDataset(d)
else:  # pointnet
    model = torch.load('pointnet.torch', map_location=device).eval()
    dset_rot = PointDataset(d, transform=tf, cloud_size=None)
    dset_normal = PointDataset(d, cloud_size=None)

# pick a random sample
N = np.random.rand()*len(dset_normal)
N = int(N)

norot,label = dset_normal[N]
print('Intended: ', classes[label])
print('No rot:   ', classify(model,norot))

print('With rot: ')
for _ in range(5):
    sample,_ = dset_rot[N]
    print(classify(model,sample))
print('*****************')