#!/usr/bin/env python3

import torch
import torch.nn.functional as F
from pyntcloud import PyntCloud
import numpy as np
import os
import sys

import rospy
from std_msgs.msg import String
from shape_msgs.msg import classification_request, classified_shape
from conversions import pc2_to_pyntcloud, voxel_converter
from sensor_msgs.msg import PointCloud2

def pc2_loader(pc2):
    vox_resolution = 24
    cloud = pc2_to_pyntcloud(pc2)
    vg = voxel_converter(cloud, vox_resolution)
    return torch.tensor(vg).view(1,1,32,32,32)


def classify(msg, model, classes):    
    data = pc2_loader(msg.cloud)
    prediction = F.softmax(model(data), -1).max(1)

    out = classified_shape()
    out.prediction = classes[prediction[1]]
    out.score = prediction[0]

    rospy.loginfo("Prediction:  %s, %f" % (out.prediction, out.score))
    return out


if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append('training/')  # to import the model
    rospy.init_node('classifier')

    classes = ['TJ0', 'TJ30', 'TJ60', 'TJ90', 'confloat', 'deepcwind', 'jacketK', 'jacketX', 'monopile', 'tripodX', 'tripodZ', 'windfloat']
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

    model = torch.load('voxnet.torch', map_location=device).eval()

    pub = rospy.Publisher('shape_completion/prediction', classified_shape, queue_size=0)
    callback = lambda msg: pub.publish(classify(msg, model, classes))
    rospy.Subscriber("shape_completion/req_cls", classification_request, callback, queue_size=1)

    rospy.spin()
