### Main Libs:
- ROS melodic
- PCL 1.9.1
- Pagmo**2**
- Pytorch 1.3.0
- PyntCloud 0.1.2

### Language Versions:
- C++14
- Python 3.6.9
