clear;close all;
loc = csvread('locations.csv')(1:1000,:);
pl = @(n)(loc( ((n-1)*100+1):n*100, : ));

centers = [0,0; 
          300,0;
          0,300;
          300,300;
          600,300;
          -300,300;
          0,-300;
          300,-300;
          -300,600;
          300,600];

dist = zeros(10,100);
angl = zeros(10,100);
for i = 1:10
  center = centers(i,:);
  points = pl(i);
  
  norm = points - center;
  dist(i,:) = sqrt(norm(:,1).^2 + norm(:,2).^2);
  %angl(i,:) = atand(norm(:,2)./norm(:,1));
  
  for j = 1:length(norm)
    s = sign(norm(j,:));
    
    if s == [1,1]
      angl(i,j) = atand(norm(j,2)./norm(j,1));
    elseif s == [1,-1]
      angl(i,j) = 90 - atand(norm(j,2)./norm(j,1));
    elseif s == [-1,1]
      angl(i,j) = -90 - atand(norm(j,2)./norm(j,1));
    elseif s == [-1,-1]
      angl(i,j) = -90 - atand(norm(j,2)./norm(j,1));
    endif
  endfor
  
  % q++ -> 0 - +90
  % q+- -> 0 - -90
  % q-+ -> 90 - 180
  % q-- -> -90 - -180
endfor

distances = dist(:);
angles = angl(:);

figure, hold on
subplot(2,1,1)
hist(distances)
xlim([0,120])
title('Distance to Center');
xlabel('Distance(m)');
ylabel('Occurrences');

subplot(2,1,2)
hist(nonzeros(angles))
xlim([-200,200])
title('Angle')
ylabel('Occurrences')
xlabel('Angle(º)')

hold off
