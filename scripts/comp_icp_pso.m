clear; close all;

icp = csvread('icp_hist.csv');
pso = csvread('pso_hist.csv')(1:500,8);

pso_pp = zeros(5, 100);
pso_pp(:) = pso; 
pso_pp = pso_pp';

pso_best = min(pso_pp, [], 2);

figure, hold on
plot([icp pso_best](1:30,:), '*-', 'linewidth', 1);
legend('ICP', 'PSO Global Best');
ylabel('Error'); xlabel('Iteration');
hold off