from pyntcloud import PyntCloud
import open3d as o3d
import numpy as np
from math import cos, sin, pi
import os

os.chdir('/home/rfdinis/catkin_ws/src/shape/ros_nodes/registration/template_10k')

def R(theta, u):
    return [[cos(theta) + u[0]**2 * (1-cos(theta)), 
             u[0] * u[1] * (1-cos(theta)) - u[2] * sin(theta), 
             u[0] * u[2] * (1 - cos(theta)) + u[1] * sin(theta)],
            [u[0] * u[1] * (1-cos(theta)) + u[2] * sin(theta),
             cos(theta) + u[1]**2 * (1-cos(theta)),
             u[1] * u[2] * (1 - cos(theta)) - u[0] * sin(theta)],
            [u[0] * u[2] * (1-cos(theta)) - u[1] * sin(theta),
             u[1] * u[2] * (1-cos(theta)) + u[0] * sin(theta),
             cos(theta) + u[2]**2 * (1-cos(theta))]]


#os.mkdir('../template_10k_fixed/')
for f in os.listdir():
    print(f)
    cloud = PyntCloud.from_file(f)
    name = os.path.splitext(f)[0]
    
    cloud.xyz = cloud.xyz - cloud.centroid
    cloud.xyz = cloud.xyz @ np.eye(3)*0.001
    cloud.xyz = cloud.xyz @ R(-pi/2, (1,0,0))
    
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.Vector3dVector(cloud.xyz)
    o3d.write_point_cloud('../template_10k_fixed/' + f, pcd)
    