clear;close all;

PARTICLES = 10;

files = ["pso_hist_3d.csv"; "pso_hist2.csv"; "hist_10p.csv"];
f = csvread("/home/rfdinis/Desktop/pso_hist.csv");% strcat("/home/rfdinis/catkin_ws/src/shape/doc/", files(1,:)));
l = length(f);

x = zeros(PARTICLES,l/PARTICLES);
y = zeros(PARTICLES,l/PARTICLES);
cost = zeros(PARTICLES, l/PARTICLES);

x(:) = f(:,1); x = x';
y(:) = f(:,2); y = y';
cost(:) = f(:,8); cost = cost';

[best_cost, best] = min(f(:,8));
best_x = f(best, 1);
best_y = f(best, 2);

%% 2D plot
figure, plot(x, y, '-', 'linewidth', 1);
hold on
plot(x(1,:), y(1,:) ,'o', 'MarkerSize', 10, 'markerfacecolor', 'red');
plot(best_x, best_y, 'p', 'MarkerSize', 20, 'markerfacecolor', 'black');
xlabel('x'); ylabel('y');
hold off

%% 3D plot
figure, plot3(x, y, cost, '-', 'linewidth', 1);
hold on
plot3(x(1,:), y(1,:), cost(1,:) ,'o', 'MarkerSize', 10, 'markerfacecolor', 'red')
xlabel('x'); ylabel('y');
zlabel('Fitness score');
hold off
