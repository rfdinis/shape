ROS_DISTRO=melodic

sudo apt -y install python-rosinstall ros-${ROS_DISTRO}-mavros-msgs ros-${ROS_DISTRO}-usv-gazebo-plugins ros-${ROS_DISTRO}-hector-gazebo ros-${ROS_DISTRO}-base-local-planner  ros-${ROS_DISTRO}-costmap-converter ros-${ROS_DISTRO}-libg2o ros-${ROS_DISTRO}-velodyne-gazebo-plugins  ros-${ROS_DISTRO}-velodyne-description ros-${ROS_DISTRO}-mavros ros-${ROS_DISTRO}-geodesy ros-${ROS_DISTRO}-robot-localization ros-${ROS_DISTRO}-nmea-msgs ros-${ROS_DISTRO}-pcl-conversions ros-${ROS_DISTRO}-pcl-ros

# must be in catkin dir
sudo rosdep install --from-paths src --ignore-src --rosdistro=${ROS_DISTRO}


sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add  
sudo apt-get update
sudo apt-get upgrade
catkin_make

#Para correr:
roslaunch zarco_sim zarco_sim.launch


