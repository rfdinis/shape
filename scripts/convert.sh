#!/bin/bash
# usage: sh convert.sh num_points
# input from "stl" folder
# output to "pcd" folder

source_dir=./stl/
out_dir=./pcd/

if ! [ -x "$(command -v ctmconv)" ]; then
  echo 'Error: openctm-tools is required.' >&2
  exit 1
fi

if ! [ -x "$(command -v pcl_mesh_sampling)" ]; then
  echo 'Error: pcl_mesh_sampling from PCL is required.' >&2
  exit 1
fi

if ! [ -d $source_dir ]; then
  echo "Directory does not exist. Create a folder called $source_dir and place stl models inside."
  exit 1
fi

if [ -d pcd ]; then
  echo "$(out_dir) directory already exists, stopping to avoid loss of data. Delete rename it to perform operation."
  exit 1
fi
mkdir -p $out_dir

echo "$(tput setaf 4)$1 Points$(tput sgr0)"


for filename in $source_dir*.stl; do
    filename="${filename%.*}"
    bname="$(basename $filename)"
    ply=$out_dir$bname.ply
    pcd=$out_dir$bname.pcd
    
    echo -e "\n$(tput setaf 2)Converting: $filename...$(tput sgr0)"
    echo "$ply"
    echo "$pcd"
    
    ctmconv $filename.stl $ply
    pcl_mesh_sampling $ply $pcd -no_vis_result -n_samples $1
    
    rm $out_dir$bname.ply
done

