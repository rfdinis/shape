close all; clear;
train = csvread('/home/rfdinis/gdrive/pointclouds11/train/locations.csv');
test  = csvread('/home/rfdinis/gdrive/pointclouds11/test/locations.csv');

yc   = 300;
xc   = -300;
rmin = 20;
rmax = 100;

circle_x = @(x,r) (r*cos(linspace(0,2*pi)) + x);
circle_y = @(y,r) (r*sin(linspace(0,2*pi)) + y);

figure
hold on

% center plot on sample
xlim([xc-rmax, xc+rmax])
ylim([yc-rmax, yc+rmax])

% plot center point
turbine = plot( [xc], [yc], 'o',        ...
                'MarkerFaceColor', 'g', ...
                'MarkerSize', 12 );
     

% plot range circles
%min_distance = plot(circle_x(xc,rmin), circle_y(yc,rmin), 'm');
%max_distance = plot(circle_x(xc,rmax), circle_y(yc,rmax), 'r');

% plot train and test points
train_points = plot(train(:,1), train(:,2), 'o', 'MarkerFaceColor', 'b');
test_points = plot(test(:,1), test(:,2), 'o', 'MarkerFaceColor', 'r');


xlabel('X Coordinate(m)')
ylabel('Y Coordinate(m)')

legend([train_points, test_points, turbine],...
        {'Train Data Origin', 'Test Data Origin', 'Turbine'})

hold off
pause;
