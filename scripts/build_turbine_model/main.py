# Script to build the directory around stl models
# to add them to gazebo
# 1- place .STL file in /stl
# 2- run main.py
# 3- results are in turbines/name 

from lxml import etree as ET
import sys
import os

POSE = '0 0 0 1.5708 0 0'
SCALE = '0.001 0.001 0.001'

TEMPLATE_SDF = 'template/model.sdf'
TEMPLATE_CONFIG = 'template/model.config'
OUT_DIR = 'turbines/'
STL_DIR = 'stl/'


def load_xml(name):
    ''' Takes an xml file as input. Outputs ElementTree and element'''
    parser = ET.XMLParser(strip_cdata=False)
    tree = ET.parse(name, parser)
    root = tree.getroot()
    return tree, root


def edit_config(name):
    tree, root = load_xml(TEMPLATE_CONFIG)
    root.find('name').text = name
    root.find('description').text = 'A ' + name + 'model.'
    tree.write(OUT_DIR + name + '/model.config',
               xml_declaration = True, encoding='UTF-8')


def edit_sdf(name):
    tree, root = load_xml(TEMPLATE_SDF)

    # convenience
    model = root.find('model')
    collision = model.findall('link')[0].findall('collision')[0]
    visual = model.findall('link')[0].findall('visual')[0]
    cmesh = collision.find('geometry').find('mesh')
    vmesh = visual.find('geometry').find('mesh')
    
    # change name
    model.set('name',name)
    # set pose
    collision.find('pose').text = POSE
    visual.find('pose').text = POSE
    # set mesh uri
    cmesh.find('uri').text = 'model://turbines/' + name + '/mesh/' + name + '.stl'
    vmesh.find('uri').text = 'model://turbines/' + name + '/mesh/' + name + '.stl'
    # set scale
    cmesh.find('scale').text = SCALE
    vmesh.find('scale').text = SCALE

    # write file
    tree.write(OUT_DIR + name + '/model.sdf',
               xml_declaration = True, encoding='UTF-8')


def setup_dir(name):
    # create directories
    os.mkdir(OUT_DIR + name)
    os.mkdir(OUT_DIR + name + '/mesh/')
    # move stl file to mesh folder
    os.rename(STL_DIR + name + '.STL',
              OUT_DIR + name + '/mesh/' + name + '.stl')


if __name__ == "__main__":
    files = os.listdir(STL_DIR)
    print(files)
    for name in files:
        name = name[:-4]  # remove extension
        print(name, flush=True)
        setup_dir(name)
        edit_config(name)
        edit_sdf(name)

