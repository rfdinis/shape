#!/bin/bash
set -e

terminal=konsole

while getopts ':ch' option; do
    case "${option}" in
        c) (cd ~/catkin_ws/ && catkin_make);;
        h) echo "-c to compile"; exit 0;;
        *) exit 1;;
    esac
done


$terminal --new-tab -e roscore &
sleep 3
$terminal --new-tab -e roslaunch zarco_sim zarco_sim.launch &
sleep 5
$terminal --new-tab -e rviz &
$terminal --new-tab -e roslaunch test_pkg demo.launch &
